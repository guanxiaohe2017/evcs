package com.ev49.evcs.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@Entity
public class AuthSecretToken {

    public final static String SECRET_TOKEN_TYPE_IN = "IN";
    public final static String SECRET_TOKEN_TYPE_OUT = "OUT";

    @Id
    @GeneratedValue()
    private Integer id;

    private String operatorId;

    private String secretTokenType;

    private String secret;

    private String token;

    private Date tokenExpiry;

    public AuthSecretToken() {
    }

    public AuthSecretToken(String operatorId, String secretTokenType) {
        this.operatorId = operatorId;
        this.secretTokenType = secretTokenType;
    }
}