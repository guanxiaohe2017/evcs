package com.ev49.evcs.dto;

import com.ev49.common.utils.json.JSONUtil;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@lombok.Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class CommonRequest<T> {

    @JsonIgnore
    @JsonProperty("id")
    Long id;
    @JsonProperty("OperatorID")
    String operatorID;
    @JsonProperty("Data")
    String data;
    @JsonProperty("TimeStamp")
    String timeStamp;
    @JsonProperty("Seq")
    String seq;
    @JsonProperty("Sig")
    String sig;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    public T anyDataType(Class<T> clz, String key) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(data);
        jsonNode = jsonNode.path(key);
        return JSONUtil.readParams(jsonNode.toString(), clz);
    }

    public T anyDataType(Class<T> clz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(data);
        return JSONUtil.readParams(jsonNode.toString(), clz);
    }

}
