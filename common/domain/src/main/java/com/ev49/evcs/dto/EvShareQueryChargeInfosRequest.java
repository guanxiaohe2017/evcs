package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class EvShareQueryChargeInfosRequest {

    @JsonProperty("BillerOperatorId")
    String billerOperatorId;

    //当该属性 为 0 时表示查询所有订单信息 当该属性为 1 时表示根据billerOperatorId查询（四九订单）
    //当该属性为 2 时表示查询四九订单信息 这时数据库字段billerOperatorId值为null
    @JsonProperty("QueryType")
    Integer queryType = 0;

    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("EndTime")
    private String endTime;
}
