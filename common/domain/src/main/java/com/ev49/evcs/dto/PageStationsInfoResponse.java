package com.ev49.evcs.dto;

import com.ev49.evcs.domain.StationInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class PageStationsInfoResponse {

    @JsonProperty("PageCount")
    Integer PageCount;
    @JsonProperty("ItemSize")
    Integer ItemSize;
    @JsonProperty(value = "PageNo", defaultValue = "1") //CAUTION: PageNo must not wrote as PageNon or anything else
            Integer PageNo = 1;
    @JsonProperty("StationInfos")
    List<StationInfo> stationInfos = new ArrayList<>();

}
