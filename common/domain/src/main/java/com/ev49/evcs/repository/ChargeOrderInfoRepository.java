package com.ev49.evcs.repository;

import com.ev49.evcs.domain.ChargeOrderInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

import java.awt.print.Book;
import java.util.Date;
import java.util.List;

import java.util.List;

@RestResource(path = "ChargeOrderInfoes", rel = "ChargeOrderInfo")
public interface ChargeOrderInfoRepository extends JpaRepository<ChargeOrderInfo, String>, QueryByExampleExecutor<ChargeOrderInfo>,JpaSpecificationExecutor<ChargeOrderInfo> {

    /**
     * 测试自定义Query用原生sql查询
     * @param seq
     * @return
     */
    @Query(nativeQuery = true,value = "select * from et_charge_order_info o where o.start_charge_seq = ?1")
    ChargeOrderInfo findByStartChargeSeq3(@Param("seq") String seq);

    /**
     * 测试使用命名函数查询数据库
     * @param startChargeSeq
     * @return
     */
    ChargeOrderInfo findChargeOrderInfoByStartChargeSeq(String startChargeSeq);

    /*List<ChargeOrderInfo> findChargeOrderInfosByStartChargeSeqLikeOrConnectorIDLike(String startChargeSeq,String connectorID);*/

    ChargeOrderInfo findFirstByOrderByStartTimeDesc();

    Page<ChargeOrderInfo> findChargeOrderInfosByBillerOperatorId(String billerOperatorId, Pageable pageable);

    Page<ChargeOrderInfo> findChargeOrderInfosByBillerOperatorIdIsNull(Pageable pageable);

    List<ChargeOrderInfo> findByEndTimeIsNullAndStartTimeIsNotNull();

    List<ChargeOrderInfo> findByEndTimeIsNotNullAndStartChargeSeq(String startChargeSeq);

    //以下四个方法是查询四九订单 统计
    @Query(value = "select sum(total_elec_money) from et_charge_order_info e where e.biller_operator_id= ?1 and e.end_time > ?2 and e.end_time < ?3", nativeQuery = true)
    Double sumSJTotalElecMoney(String operaterID,String startTime,String endTime);

    @Query(value = "select sum(total_money) from et_charge_order_info e where e.biller_operator_id= ?1 and e.end_time > ?2 and e.end_time < ?3", nativeQuery = true)
    Double sumSJTotalMoney(String operaterID,String startTime,String endTime);

    @Query(value = "select sum(total_power) from et_charge_order_info e where e.biller_operator_id= ?1 and e.end_time > ?2 and e.end_time < ?3", nativeQuery = true)
    Double sumSJTotalPower(String operaterID,String startTime,String endTime);

    @Query(value = "select sum(total_sevice_money) from et_charge_order_info e where e.biller_operator_id= ?1 and e.end_time > ?2 and e.end_time < ?3", nativeQuery = true)
    Double sumSJTotalServiceMoney(String operaterID,String startTime,String endTime);

    //以下四个方法是查询一电订单 统计
    @Query(value = "select sum(total_elec_money) from et_charge_order_info e where e.biller_operator_id is null and e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumYDTotalElecMoney(String startTime,String endTime);

    @Query(value = "select sum(total_money) from et_charge_order_info e where e.biller_operator_id is null and e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumYDTotalMoney(String startTime,String endTime);

    @Query(value = "select sum(total_power) from et_charge_order_info e where e.biller_operator_id is null and e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumYDTotalPower(String startTime,String endTime);

    @Query(value = "select sum(total_sevice_money) from et_charge_order_info e where e.biller_operator_id is null and e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumYDTotalServiceMoney(String startTime,String endTime);

    //以下四个方法是查询所有订单 统计
    @Query(value = "select sum(total_elec_money) from et_charge_order_info e where e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumAllTotalElecMoney(String startTime,String endTime);

    @Query(value = "select sum(total_money) from et_charge_order_info e where e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumAllTotalMoney(String startTime,String endTime);

    @Query(value = "select sum(total_power) from et_charge_order_info e where e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumAllTotalPower(String startTime,String endTime);

    @Query(value = "select sum(total_sevice_money) from et_charge_order_info e where e.end_time >  ?1 and e.end_time < ?2", nativeQuery = true)
    Double sumAllTotalServiceMoney(String startTime,String endTime);
}
