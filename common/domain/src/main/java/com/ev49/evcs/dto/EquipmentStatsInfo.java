package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "EquipmentID",
        "EquipmentElectricity",
        "ConnectorStatsInfos"
})
public class EquipmentStatsInfo {

    @JsonProperty("EquipmentID")
    private String equipmentID;
    @JsonProperty("EquipmentElectricity")
    private Double equipmentElectricity;
    @JsonProperty("ConnectorStatsInfos")
    private List<ConnectorStatsInfo> connectorStatsInfos = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("EquipmentID")
    public String getEquipmentID() {
        return equipmentID;
    }

    @JsonProperty("EquipmentID")
    public void setEquipmentID(String equipmentID) {
        this.equipmentID = equipmentID;
    }

    @JsonProperty("EquipmentElectricity")
    public Double getEquipmentElectricity() {
        return equipmentElectricity;
    }

    @JsonProperty("EquipmentElectricity")
    public void setEquipmentElectricity(Double equipmentElectricity) {
        this.equipmentElectricity = equipmentElectricity;
    }

    @JsonProperty("ConnectorStatsInfos")
    public List<ConnectorStatsInfo> getConnectorStatsInfos() {
        return connectorStatsInfos;
    }

    @JsonProperty("ConnectorStatsInfos")
    public void setConnectorStatsInfos(List<ConnectorStatsInfo> connectorStatsInfos) {
        this.connectorStatsInfos = connectorStatsInfos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}