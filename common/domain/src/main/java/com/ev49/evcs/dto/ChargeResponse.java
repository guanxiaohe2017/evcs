package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "SuccStat",
        "FailReason",
        "ConnectorID",
        "StartChargeSeqStat",
        "StartChargeSeq"
})
public class ChargeResponse {

    @JsonProperty("SuccStat")
    private Integer succStat;

    //0 无；
    //1 此设备不存在
    //2 此设备离线：
    //其它 自定义
    @JsonProperty("FailReason")
    private Integer failReason;
    @JsonProperty("ConnectorID")
    private String connectorID;

    //1 启动中
    //2 充电中
    //3 停止中
    //4 已结束
    //其它值 未知
    @JsonProperty("StartChargeSeqStat")
    private Integer startChargeSeqStat;
    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("SuccStat")
    public Integer getSuccStat() {
        return succStat;
    }

    @JsonProperty("SuccStat")
    public void setSuccStat(Integer succStat) {
        this.succStat = succStat;
    }

    @JsonProperty("FailReason")
    public Integer getFailReason() {
        return failReason;
    }

    @JsonProperty("FailReason")
    public void setFailReason(Integer failReason) {
        this.failReason = failReason;
    }

    @JsonProperty("ConnectorID")
    public String getConnectorID() {
        return connectorID;
    }

    @JsonProperty("ConnectorID")
    public void setConnectorID(String connectorID) {
        this.connectorID = connectorID;
    }

    @JsonProperty("StartChargeSeqStat")
    public Integer getStartChargeSeqStat() {
        return startChargeSeqStat;
    }

    @JsonProperty("StartChargeSeqStat")
    public void setStartChargeSeqStat(Integer startChargeSeqStat) {
        this.startChargeSeqStat = startChargeSeqStat;
    }

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("succStat", succStat).append("failReason", failReason).append("connectorID", connectorID).append("startChargeSeqStat", startChargeSeqStat).append("startChargeSeq", startChargeSeq).append("additionalProperties", additionalProperties).toString();
    }

}