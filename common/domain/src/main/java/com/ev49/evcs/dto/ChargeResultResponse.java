package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@lombok.Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
/**
 * 推送停止充电结果返回数据dto类
 */
public class ChargeResultResponse {

    @JsonProperty("StartChargeSeq")
    String startChargeSeq;
    @JsonProperty("SuccStat")
    Integer succStat;
    @JsonProperty("FailReason")
    Integer failReason;

}
