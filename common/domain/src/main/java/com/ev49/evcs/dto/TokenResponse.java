package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "FailReason",
        "OperatorID",
        "SuccStat",
        "AccessToken",
        "TokenAvailableTime"
})
public class TokenResponse {

    @JsonProperty("FailReason")
    private Integer failReason;
    @JsonProperty("OperatorID")
    private String operatorID;
    @JsonProperty("SuccStat")
    private Integer succStat;
    @JsonProperty("AccessToken")
    private String accessToken;
    @JsonProperty("TokenAvailableTime")
    private Integer tokenAvailableTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("FailReason")
    public Integer getFailReason() {
        return failReason;
    }

    @JsonProperty("FailReason")
    public void setFailReason(Integer failReason) {
        this.failReason = failReason;
    }

    @JsonProperty("OperatorID")
    public String getOperatorID() {
        return operatorID;
    }

    @JsonProperty("OperatorID")
    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    @JsonProperty("SuccStat")
    public Integer getSuccStat() {
        return succStat;
    }

    @JsonProperty("SuccStat")
    public void setSuccStat(Integer succStat) {
        this.succStat = succStat;
    }

    @JsonProperty("AccessToken")
    public String getAccessToken() {
        return accessToken;
    }

    @JsonProperty("AccessToken")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @JsonProperty("TokenAvailableTime")
    public Integer getTokenAvailableTime() {
        return tokenAvailableTime;
    }

    @JsonProperty("TokenAvailableTime")
    public void setTokenAvailableTime(Integer tokenAvailableTime) {
        this.tokenAvailableTime = tokenAvailableTime;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("failReason", failReason).append("operatorID", operatorID).append("succStat", succStat).append("accessToken", accessToken).append("tokenAvailableTime", tokenAvailableTime).append("additionalProperties", additionalProperties).toString();
    }

}