package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeq",
        "StartChargeSeqStat",
        "ConnectorID",
        "StartTime",
        "IdentCode"
})
public class NotificationStartChargeResultRequestData {

    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonProperty("StartChargeSeqStat")
    private Integer startChargeSeqStat;
    @JsonProperty("ConnectorID")
    private String connectorId;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("IdentCode")
    private String identCode;

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("EndTime")
    public Integer getStartChargeSeqStat() {
        return startChargeSeqStat;
    }

    @JsonProperty("EndTime")
    public void setStartChargeSeqStat(Integer startChargeSeqStat) {
        this.startChargeSeqStat = startChargeSeqStat;
    }

    @JsonProperty("ConnectorID")
    public String getConnectorId() {
        return connectorId;
    }

    @JsonProperty("ConnectorID")
    public void setConnectorId(String connectorId) {
        this.connectorId = connectorId;
    }

    @JsonProperty("IdentCode")
    public String getIdentCode() {
        return identCode;
    }

    @JsonProperty("IdentCode")
    public void setIdentCode(String identCode) {
        this.identCode = identCode;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeq", startChargeSeq).append("startTime", startTime).append("startChargeSeqStat", startChargeSeqStat).append("connectorId", connectorId).toString();
    }

}
