package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class QueryTokenResponse {

    @JsonProperty("OperatorID")
    String operatorId;
    @JsonProperty("SuccStat")
    Integer succStat;
    @JsonProperty("AccessToken")
    String accessToken;
    @JsonProperty("TokenAvailableTime")
    Integer tokenAvailableTime;
    @JsonProperty("FailReason")
    Integer failReason;
}
