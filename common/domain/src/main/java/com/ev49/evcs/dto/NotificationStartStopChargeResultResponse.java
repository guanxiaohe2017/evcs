package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "SuccStat",
        "FailReason",
        "StartChargeSeq"
})
public class NotificationStartStopChargeResultResponse {

    @JsonProperty("SuccStat")
    private Integer succStat;
    @JsonProperty("FailReason")
    private Integer failReason;
    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("SuccStat")
    public Integer getSuccStat() {
        return succStat;
    }

    @JsonProperty("SuccStat")
    public void setSuccStat(Integer succStat) {
        this.succStat = succStat;
    }

    @JsonProperty("FailReason")
    public Integer getFailReason() {
        return failReason;
    }

    @JsonProperty("FailReason")
    public void setFailReason(Integer failReason) {
        this.failReason = failReason;
    }

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("succStat", succStat).append("failReason", failReason).append("startChargeSeq", startChargeSeq).append("additionalProperties", additionalProperties).toString();
    }

}