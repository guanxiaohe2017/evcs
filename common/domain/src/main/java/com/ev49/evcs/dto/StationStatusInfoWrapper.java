package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Total",
        "StationStatusInfos"
})
public class StationStatusInfoWrapper {

    @JsonProperty("Total")
    private Integer total;
    @JsonProperty("StationStatusInfos")
    private List<StationStatusInfo> stationStatusInfos = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("Total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @JsonProperty("StationStatusInfos")
    public List<StationStatusInfo> getStationStatusInfos() {
        return stationStatusInfos;
    }

    @JsonProperty("StationStatusInfos")
    public void setStationStatusInfos(List<StationStatusInfo> stationStatusInfos) {
        this.stationStatusInfos = stationStatusInfos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("total", total).append("stationStatusInfos", stationStatusInfos).append("additionalProperties", additionalProperties).toString();
    }

}