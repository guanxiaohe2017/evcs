package com.ev49.evcs.repository;

import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.domain.StationInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path = "ConnectorStatusInfos", rel = "ConnectorStatusInfo")
public interface ConnectorStatusInfoRepository extends JpaRepository<ConnectorStatusInfo, String>, QueryByExampleExecutor<ConnectorStatusInfo> {

}
