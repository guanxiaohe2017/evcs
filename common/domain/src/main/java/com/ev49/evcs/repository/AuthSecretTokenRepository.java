package com.ev49.evcs.repository;

import com.ev49.evcs.domain.AuthSecretToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface AuthSecretTokenRepository extends JpaRepository<AuthSecretToken, String>, QueryByExampleExecutor<AuthSecretToken>, JpaSpecificationExecutor<AuthSecretToken> {


    Optional<AuthSecretToken> findByOperatorIdAndSecretTokenType(String operatorId, String secretTokenType);

    Optional<AuthSecretToken> findByOperatorIdAndSecretTokenTypeAndTokenExpiryGreaterThan(String operatorId, String secretTokenType, Date time);
}
