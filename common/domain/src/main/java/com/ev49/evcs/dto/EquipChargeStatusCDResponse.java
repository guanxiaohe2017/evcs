package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EquipChargeStatusCDResponse {

    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonProperty("StartChargeSeqStat")
    private Integer startChargeSeqStat = 4;
    @JsonProperty("ConnectorID")
    private String connectorID;
    @JsonProperty("ConnectorStatus")
    private Integer connectorStatus = 1;
    @JsonProperty("CurrentA")
    private Double currentA = 15.0;
    @JsonProperty("CurrentB")
    private Double currentB = 15.0;
    @JsonProperty("CurrentC")
    private Double currentC = 15.0;
    @JsonProperty("VoltageA")
    private Double voltageA = 220.0;
    @JsonProperty("VoltageB")
    private Double voltageB = 220.0;
    @JsonProperty("VoltageC")
    private Double voltageC = 220.0;
    @JsonProperty("Soc")
    private Double soc = 0.0;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("ChargeModel")
    private Integer chargeModel = 0;
    @JsonProperty("EndTime")
    private String endTime;
    @JsonProperty("TotalPower")
    private Double totalPower = 0.0;
    @JsonProperty("ElecMoney")
    private Double elecMoney = 0.0;
    @JsonProperty("SeviceMoney")
    private Double seviceMoney = 0.0;
    @JsonProperty("TotalMoney")
    private Double totalMoney = 0.0;
    @JsonProperty("SumPeriod")
    private Integer sumPeriod = 0;
    @JsonProperty("ChargeDetails")
    private ChargeDetails[] chargeDetails;
}