package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StationID",
        "StartTime",
        "EndTime",
        "StationElectricity",
        "EquipmentStatsInfos"
})
public class StationStatsInfo {

    @JsonProperty("StationID")
    private String stationID;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("EndTime")
    private String endTime;
    @JsonProperty("StationElectricity")
    private Double stationElectricity;
    @JsonProperty("EquipmentStatsInfos")
    private List<EquipmentStatsInfo> equipmentStatsInfos = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StationID")
    public String getStationID() {
        return stationID;
    }

    @JsonProperty("StationID")
    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("EndTime")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("EndTime")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("StationElectricity")
    public Double getStationElectricity() {
        return stationElectricity;
    }

    @JsonProperty("StationElectricity")
    public void setStationElectricity(Double stationElectricity) {
        this.stationElectricity = stationElectricity;
    }

    @JsonProperty("EquipmentStatsInfos")
    public List<EquipmentStatsInfo> getEquipmentStatsInfos() {
        return equipmentStatsInfos;
    }

    @JsonProperty("EquipmentStatsInfos")
    public void setEquipmentStatsInfos(List<EquipmentStatsInfo> equipmentStatsInfos) {
        this.equipmentStatsInfos = equipmentStatsInfos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}