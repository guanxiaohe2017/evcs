package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class PageResponse {

    @JsonProperty("PageCount")
    Integer PageCount;
    @JsonProperty("ItemSize")
    Integer ItemSize;
    @JsonProperty(value = "PageNo", defaultValue = "1") //CAUTION: PageNo must not wrote as PageNon or anything else
            Integer PageNo = 1;
    @JsonProperty(value = "PageSize", defaultValue = "10")
    Integer PageSize = 10;
}
