package com.ev49.evcs.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class CommonStationInfo {

    @Id
    @JsonProperty("StationID")
    String stationID;
    @JsonProperty("OperatorID")
    String operatorID;
    @JsonProperty("EquipmentOwnerID")
    String equipmentOwnerID;
    @JsonProperty("StationName")
    String stationName;
    @JsonProperty("CountryCode")
    String countryCode;

}
