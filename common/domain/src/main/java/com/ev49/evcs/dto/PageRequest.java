package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class PageRequest {

    @JsonProperty("LastQueryTime")
    String lastQueryTime;
    @JsonProperty(value = "PageNo", defaultValue = "1") //CAUTION: PageNo must not wrote as PageNon or anything else
            Integer pageNo = 1;
    @JsonProperty(value = "PageSize", defaultValue = "10")
    Integer pageSize = 10;
}
