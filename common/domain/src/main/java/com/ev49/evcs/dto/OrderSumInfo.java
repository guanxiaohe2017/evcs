package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class OrderSumInfo {

    @JsonProperty("AllElectric")
    BigDecimal allElectric;

    @JsonProperty("AllElectricMoney")
    BigDecimal allElectricMoney;

    @JsonProperty("AllServiceMoney")
    BigDecimal allServiceMoney;

    @JsonProperty("AllMoney")
    BigDecimal allMoney;

    public OrderSumInfo(BigDecimal allElectric, BigDecimal allElectricMoney, BigDecimal allServiceMoney, BigDecimal allMoney) {
        this.allElectric = allElectric;
        this.allElectricMoney = allElectricMoney;
        this.allServiceMoney = allServiceMoney;
        this.allMoney = allMoney;
    }

    public OrderSumInfo() {
    }
}
