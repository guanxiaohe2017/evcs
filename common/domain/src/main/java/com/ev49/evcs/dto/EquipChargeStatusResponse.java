package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeqStat",
        "ConnectorStatus",
        "CurrentA",
        "CurrentB",
        "CurrentC",
        "VoltageA",
        "VoltageB",
        "VoltageC",
        "Soc",
        "TotalPower",
        "ElecMoney",
        "SeviceMoney",
        "TotalMoney",
        "SumPeriod",
        "StartTime"
})
public class EquipChargeStatusResponse {

    @JsonProperty("StartChargeSeqStat")
    private Integer startChargeSeqStat;
    @JsonProperty("ConnectorStatus")
    private Integer connectorStatus;
    @JsonProperty("CurrentA")
    private Double currentA;
    @JsonProperty("CurrentB")
    private Double currentB;
    @JsonProperty("CurrentC")
    private Double currentC;
    @JsonProperty("VoltageA")
    private Double voltageA;
    @JsonProperty("VoltageB")
    private Double voltageB;
    @JsonProperty("VoltageC")
    private Double voltageC;
    @JsonProperty("Soc")
    private Double soc;
    @JsonProperty("TotalPower")
    private Double totalPower;
    @JsonProperty("ElecMoney")
    private Double elecMoney;
    @JsonProperty("SeviceMoney")
    private Double seviceMoney;
    @JsonProperty("TotalMoney")
    private Double totalMoney;
    @JsonProperty("SumPeriod")
    private Integer sumPeriod;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("StartChargeSeqStat")
    public Integer getStartChargeSeqStat() {
        return startChargeSeqStat;
    }

    @JsonProperty("StartChargeSeqStat")
    public void setStartChargeSeqStat(Integer startChargeSeqStat) {
        this.startChargeSeqStat = startChargeSeqStat;
    }

    @JsonProperty("ConnectorStatus")
    public Integer getConnectorStatus() {
        return connectorStatus;
    }

    @JsonProperty("ConnectorStatus")
    public void setConnectorStatus(Integer connectorStatus) {
        this.connectorStatus = connectorStatus;
    }

    @JsonProperty("CurrentA")
    public Double getCurrentA() {
        return currentA;
    }

    @JsonProperty("CurrentA")
    public void setCurrentA(Double currentA) {
        this.currentA = currentA;
    }

    @JsonProperty("CurrentB")
    public Double getCurrentB() {
        return currentB;
    }

    @JsonProperty("CurrentB")
    public void setCurrentB(Double currentB) {
        this.currentB = currentB;
    }

    @JsonProperty("CurrentC")
    public Double getCurrentC() {
        return currentC;
    }

    @JsonProperty("CurrentC")
    public void setCurrentC(Double currentC) {
        this.currentC = currentC;
    }

    @JsonProperty("VoltageA")
    public Double getVoltageA() {
        return voltageA;
    }

    @JsonProperty("VoltageA")
    public void setVoltageA(Double voltageA) {
        this.voltageA = voltageA;
    }

    @JsonProperty("VoltageB")
    public Double getVoltageB() {
        return voltageB;
    }

    @JsonProperty("VoltageB")
    public void setVoltageB(Double voltageB) {
        this.voltageB = voltageB;
    }

    @JsonProperty("VoltageC")
    public Double getVoltageC() {
        return voltageC;
    }

    @JsonProperty("VoltageC")
    public void setVoltageC(Double voltageC) {
        this.voltageC = voltageC;
    }

    @JsonProperty("Soc")
    public Double getSoc() {
        return soc;
    }

    @JsonProperty("Soc")
    public void setSoc(Double soc) {
        this.soc = soc;
    }

    @JsonProperty("TotalPower")
    public Double getTotalPower() {
        return totalPower;
    }

    @JsonProperty("TotalPower")
    public void setTotalPower(Double totalPower) {
        this.totalPower = totalPower;
    }

    @JsonProperty("ElecMoney")
    public Double getElecMoney() {
        return elecMoney;
    }

    @JsonProperty("ElecMoney")
    public void setElecMoney(Double elecMoney) {
        this.elecMoney = elecMoney;
    }

    @JsonProperty("SeviceMoney")
    public Double getSeviceMoney() {
        return seviceMoney;
    }

    @JsonProperty("SeviceMoney")
    public void setSeviceMoney(Double seviceMoney) {
        this.seviceMoney = seviceMoney;
    }

    @JsonProperty("TotalMoney")
    public Double getTotalMoney() {
        return totalMoney;
    }

    @JsonProperty("TotalMoney")
    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    @JsonProperty("SumPeriod")
    public Integer getSumPeriod() {
        return sumPeriod;
    }

    @JsonProperty("SumPeriod")
    public void setSumPeriod(Integer sumPeriod) {
        this.sumPeriod = sumPeriod;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeqStat", startChargeSeqStat).append("connectorStatus", connectorStatus).append("currentA", currentA).append("currentB", currentB).append("currentC", currentC).append("voltageA", voltageA).append("voltageB", voltageB).append("voltageC", voltageC).append("soc", soc).append("totalPower", totalPower).append("elecMoney", elecMoney).append("seviceMoney", seviceMoney).append("totalMoney", totalMoney).append("sumPeriod", sumPeriod).append("additionalProperties", additionalProperties).toString();
    }

}