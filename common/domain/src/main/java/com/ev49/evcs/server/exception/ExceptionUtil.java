package com.ev49.evcs.server.exception;

import org.slf4j.Logger;

@Deprecated
public class ExceptionUtil {

    /**
     * format & log exception message
     */
    public static String fpmsg(Exception e, Logger log) {
        String msg = fmt(e);
        error(log, msg);
        return msg;
    }

    /**
     * format & log exception message with customized msg
     */
    public static String fpmsg(Exception e, Logger log, String msg) {
        msg += "|" + fmt(e);
        error(log, msg);
        return msg;
    }

    public static String fpmsg(Exception e, Logger log, String msg, StackTraceElement threadStackTraceElement) {
        String className = threadStackTraceElement.getClassName();
        int lineNumber = threadStackTraceElement.getLineNumber();
        msg += "|" + fmt(e) + "|" + className + "(" + lineNumber + ")";
        error(log, msg);
        return msg;
    }

    public static String fpmsg(Exception e, Logger log, StackTraceElement threadStackTraceElement) {
        String className = threadStackTraceElement.getClassName();
        int lineNumber = threadStackTraceElement.getLineNumber();
        String msg = fmt(e);
        msg += "|" + className + "(" + lineNumber + ")";
        error(log, msg);
        return msg;
    }

    private static String fmt(Exception e) {
        StackTraceElement stackTraceElement = e.getStackTrace()[0];
        return stackTraceElement.getClassName() + "(" + stackTraceElement.getLineNumber() + ") " + e.getMessage();
    }

    private static void error(Logger log, String msg) {
        log.error(msg);
    }

}
