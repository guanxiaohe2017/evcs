package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ConnectorID",
        "ConnectorElectricity"
})
public class ConnectorStatsInfo {

    @JsonProperty("ConnectorID")
    private String connectorID;
    @JsonProperty("ConnectorElectricity")
    private Double connectorElectricity;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ConnectorID")
    public String getConnectorID() {
        return connectorID;
    }

    @JsonProperty("ConnectorID")
    public void setConnectorID(String connectorID) {
        this.connectorID = connectorID;
    }

    @JsonProperty("ConnectorElectricity")
    public Double getConnectorElectricity() {
        return connectorElectricity;
    }

    @JsonProperty("ConnectorElectricity")
    public void setConnectorElectricity(Double connectorElectricity) {
        this.connectorElectricity = connectorElectricity;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}