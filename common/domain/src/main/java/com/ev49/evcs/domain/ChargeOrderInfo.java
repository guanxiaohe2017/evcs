package com.ev49.evcs.domain;

import com.ev49.evcs.dto.ChargeDetails;
import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeq",
        "ConnectorID",
        "StartTime",
        "EndTime",
        "TotalPower",
        "TotalElecMoney",
        "TotalSeviceMoney",
        "TotalMoney",
        "StopReason",
        "SumPeriod"
})
@Entity
@Setter
@Getter
@Table(name = "ET_CHARGE_ORDER_INFO")
public class ChargeOrderInfo {

    @Id
    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonIgnore
    private Integer startChargeSeqStat;
    @JsonIgnore
    private Integer failReason;
    @JsonIgnore
    private String identCode;
    @JsonIgnore
    @Column(updatable = false)
    private String infraOperatorId;
    @JsonIgnore
    @Column(updatable = false)
    private String billerOperatorId;//biller
    @JsonProperty("ConnectorID")
    private String connectorID;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("ChargeModel")
    private Integer chargeModel;
    @JsonProperty("Vin")
    private String vin;
    @JsonProperty("EndTime")
    private String endTime;
    @JsonProperty("TotalPower")
    @Column(columnDefinition = "Decimal(10,2)")
    private Double totalPower = 0.0;
    @JsonProperty("TotalElecMoney")
    @Column(columnDefinition = "Decimal(10,2)")
    private Double totalElecMoney = 0.0;
    @JsonProperty("TotalSeviceMoney")
    @Column(columnDefinition = "Decimal(10,2)")
    private Double totalSeviceMoney = 0.0;
    @JsonProperty("TotalMoney")
    @Column(columnDefinition = "Decimal(10,2)")
    private Double totalMoney = 0.0;
    @JsonProperty("StopReason")
    private Integer stopReason;
    @JsonProperty("SumPeriod")
    private Integer sumPeriod;
    @JsonProperty("ChargeDetails")
    private ChargeDetails[] chargeDetails;
    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    public ChargeOrderInfo setStartChargeSeqAndReturn(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
        return this;
    }

    @JsonProperty("ConnectorID")
    public String getConnectorID() {
        return connectorID;
    }

    @JsonProperty("ConnectorID")
    public void setConnectorID(String connectorID) {
        this.connectorID = connectorID;
    }

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("EndTime")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("EndTime")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("TotalPower")
    public Double getTotalPower() {
        return totalPower;
    }

    @JsonProperty("TotalPower")
    public void setTotalPower(Double totalPower) {
        this.totalPower = totalPower;
    }

    @JsonProperty("TotalElecMoney")
    public Double getTotalElecMoney() {
        return totalElecMoney;
    }

    @JsonProperty("TotalElecMoney")
    public void setTotalElecMoney(Double totalElecMoney) {
        this.totalElecMoney = totalElecMoney;
    }

    @JsonProperty("TotalSeviceMoney")
    public Double getTotalSeviceMoney() {
        return totalSeviceMoney;
    }

    @JsonProperty("TotalSeviceMoney")
    public void setTotalSeviceMoney(Double totalSeviceMoney) {
        this.totalSeviceMoney = totalSeviceMoney;
    }

    @JsonProperty("TotalMoney")
    public Double getTotalMoney() {
        return totalMoney;
    }

    @JsonProperty("TotalMoney")
    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    @JsonProperty("StopReason")
    public Integer getStopReason() {
        return stopReason;
    }

    @JsonProperty("StopReason")
    public void setStopReason(Integer stopReason) {
        this.stopReason = stopReason;
    }

    @JsonProperty("SumPeriod")
    public Integer getSumPeriod() {
        return sumPeriod;
    }

    @JsonProperty("SumPeriod")
    public void setSumPeriod(Integer sumPeriod) {
        this.sumPeriod = sumPeriod;
    }

    @JsonAnyGetter
    @Transient
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    @Transient
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeq", startChargeSeq).append("connectorID", connectorID).append("startTime", startTime).append("endTime", endTime).append("totalPower", totalPower).append("totalElecMoney", totalElecMoney).append("totalSeviceMoney", totalSeviceMoney).append("totalMoney", totalMoney).append("stopReason", stopReason).append("sumPeriod", sumPeriod).append("additionalProperties", additionalProperties).toString();
    }

}