package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeq",
        "TotalPower",
        "TotalMoney",
        "StartChargeSeq"
})
public class ChargeOrder {

    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonProperty("TotalPower")
    private Integer totalPower;
    @JsonProperty("TotalMoney")
    private Integer totalMoney;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonProperty("TotalPower")
    public Integer getTotalPower() {
        return totalPower;
    }

    @JsonProperty("TotalPower")
    public void setTotalPower(Integer totalPower) {
        this.totalPower = totalPower;
    }

    @JsonProperty("TotalMoney")
    public Integer getTotalMoney() {
        return totalMoney;
    }

    @JsonProperty("TotalMoney")
    public void setTotalMoney(Integer totalMoney) {
        this.totalMoney = totalMoney;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeq", startChargeSeq).append("totalPower", totalPower).append("totalMoney", totalMoney).append("startChargeSeq", startChargeSeq).append("additionalProperties", additionalProperties).toString();
    }

}
