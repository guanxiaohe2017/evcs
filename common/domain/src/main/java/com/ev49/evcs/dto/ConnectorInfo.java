package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "NationalStandard",
        "ConnectorID",
        "ConnectorName",
        "ConnectorType",
        "VoltageUpperLimits",
        "VoltageLowerLimits",
        "Current",
        "Power"
})
@Setter@Getter
public class ConnectorInfo {

    @JsonProperty("NationalStandard")
    public Long nationalStandard;
    @JsonProperty("ConnectorID")
    public String connectorID;
    @JsonProperty("ConnectorName")
    public String connectorName;
    @JsonProperty("ConnectorType")
    public Long connectorType;
    @JsonProperty("VoltageUpperLimits")
    public Long voltageUpperLimits;
    @JsonProperty("VoltageLowerLimits")
    public Long voltageLowerLimits;
    @JsonProperty("Current")
    public Long current;
    @JsonProperty("Power")
    public Double power;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("nationalStandard", nationalStandard).append("connectorID", connectorID).append("connectorName", connectorName).append("connectorType", connectorType).append("voltageUpperLimits", voltageUpperLimits).append("voltageLowerLimits", voltageLowerLimits).append("current", current).append("power", power).append("additionalProperties", additionalProperties).toString();
    }

}