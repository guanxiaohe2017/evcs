package com.ev49.evcs.repository;

import com.ev49.evcs.domain.StationInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path = "StationInfos", rel = "StationInfo")
public interface StationInfoRepository extends JpaRepository<StationInfo, String>, QueryByExampleExecutor<StationInfo> {

}
