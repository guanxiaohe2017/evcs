package com.ev49.evcs.domain;

import com.ev49.evcs.dto.EquipmentInfo;
import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StationID",
        "OperatorID",
        "EquipmentOwnerID",
        "StationName",
        "CountryCode",
        "AreaCode",
        "Address",
        "ServiceTel",
        "StationType",
        "StationStatus",
        "ParkNums",
        "StationLng",
        "StationLat",
        "Construction",
        "BusineHours",
        "ElectricityFee",
        "ServiceFee",
        "Payment",
        "SupportOrder",
        "EquipmentInfos"
})
@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
public class StationInfo extends CommonStationInfo {

    @Transient
    @JsonProperty("AreaCode")
    public String areaCode;
    @Transient
    @JsonProperty("Address")
    public String address;
    @Transient
    @JsonProperty("ServiceTel")
    public String serviceTel;
    @Transient
    @JsonProperty("StationType")
    public Long stationType;
    @Transient
    @JsonProperty("StationStatus")
    public Long stationStatus;
    @Transient
    @JsonProperty("ParkNums")
    public Long parkNums;
    @Transient
    @JsonProperty("StationLng")
    public Double stationLng;
    @Transient
    @JsonProperty("StationLat")
    public Double stationLat;
    @Transient
    @JsonProperty("Construction")
    public Long construction;
    @Transient
    @JsonProperty("BusineHours")
    public String busineHours;
    @Transient
    @JsonProperty("ElectricityFee")
    public String electricityFee;
    @Transient
    @JsonProperty("ServiceFee")
    public String serviceFee;
    @Transient
    @JsonProperty("Payment")
    public String payment;
    @Transient
    @JsonProperty("SupportOrder")
    public Long supportOrder;
    @Transient
    @JsonProperty("EquipmentInfos")
    public List<EquipmentInfo> equipmentInfos = null;
    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("stationID", getStationID()).append("operatorID", getOperatorID()).append("equipmentOwnerID", getEquipmentOwnerID()).append("stationName", getStationName()).append("countryCode", getCountryCode()).append("areaCode", areaCode).append("address", address).append("serviceTel", serviceTel).append("stationType", stationType).append("stationStatus", stationStatus).append("parkNums", parkNums).append("stationLng", stationLng).append("stationLat", stationLat).append("construction", construction).append("busineHours", busineHours).append("electricityFee", electricityFee).append("serviceFee", serviceFee).append("payment", payment).append("supportOrder", supportOrder).append("equipmentInfos", equipmentInfos).append("additionalProperties", additionalProperties).toString();
    }

}