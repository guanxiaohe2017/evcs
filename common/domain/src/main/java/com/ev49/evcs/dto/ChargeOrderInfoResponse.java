package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeq",
        "ConnectorID",
        "ConfirmResult"
})
public class ChargeOrderInfoResponse {

    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonProperty("ConnectorID")
    private String connectorID;
    @JsonProperty("ConfirmResult")
    private Integer confirmResult;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonProperty("ConnectorID")
    public String getConnectorID() {
        return connectorID;
    }

    @JsonProperty("ConnectorID")
    public void setConnectorID(String connectorID) {
        this.connectorID = connectorID;
    }

    @JsonProperty("ConfirmResult")
    public Integer getConfirmResult() {
        return confirmResult;
    }

    @JsonProperty("ConfirmResult")
    public void setConfirmResult(Integer confirmResult) {
        this.confirmResult = confirmResult;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeq", startChargeSeq).append("connectorID", connectorID).append("confirmResult", confirmResult).append("additionalProperties", additionalProperties).toString();
    }

}