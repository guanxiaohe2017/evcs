package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CheckOrderSeq",
        "StartTime",
        "EndTime",
        "OrderCount",
        "TotalOrderPower",
        "TotalOrderMoney",
        "ChargeOrders"
})
public class CheckChargeOrderRequestData {

    @JsonProperty("CheckOrderSeq")
    private String checkOrderSeq;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("EndTime")
    private String endTime;
    @JsonProperty("OrderCount")
    private Integer orderCount;
    @JsonProperty("TotalOrderPower")
    private Integer totalOrderPower;
    @JsonProperty("TotalOrderMoney")
    private Integer totalOrderMoney;
    @JsonProperty("ChargeOrders")
    private List<ChargeOrder> chargeOrders = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CheckOrderSeq")
    public String getCheckOrderSeq() {
        return checkOrderSeq;
    }

    @JsonProperty("CheckOrderSeq")
    public void setCheckOrderSeq(String checkOrderSeq) {
        this.checkOrderSeq = checkOrderSeq;
    }

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("EndTime")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("EndTime")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("OrderCount")
    public Integer getOrderCount() {
        return orderCount;
    }

    @JsonProperty("OrderCount")
    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    @JsonProperty("TotalOrderPower")
    public Integer getTotalOrderPower() {
        return totalOrderPower;
    }

    @JsonProperty("TotalOrderPower")
    public void setTotalOrderPower(Integer totalOrderPower) {
        this.totalOrderPower = totalOrderPower;
    }

    @JsonProperty("TotalOrderMoney")
    public Integer getTotalOrderMoney() {
        return totalOrderMoney;
    }

    @JsonProperty("TotalOrderMoney")
    public void setTotalOrderMoney(Integer totalOrderMoney) {
        this.totalOrderMoney = totalOrderMoney;
    }

    @JsonProperty("ChargeOrders")
    public List<ChargeOrder> getChargeOrders() {
        return chargeOrders;
    }

    @JsonProperty("ChargeOrders")
    public void setChargeOrders(List<ChargeOrder> chargeOrders) {
        this.chargeOrders = chargeOrders;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("checkOrderSeq", checkOrderSeq).append("startTime", startTime).append("endTime", endTime).append("orderCount", orderCount).append("totalOrderPower", totalOrderPower).append("totalOrderMoney", totalOrderMoney).append("chargeOrders", chargeOrders).append("additionalProperties", additionalProperties).toString();
    }

}
