package com.ev49.evcs.dto;

import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StationID",
        "ConnectorStatusInfos"
})
public class StationStatusInfo {

    @JsonProperty("StationID")
    private String stationID;
    @JsonProperty("ConnectorStatusInfos")
    private List<ConnectorStatusInfo> connectorStatusInfos = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StationID")
    public String getStationID() {
        return stationID;
    }

    @JsonProperty("StationID")
    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    @JsonProperty("ConnectorStatusInfos")
    public List<ConnectorStatusInfo> getConnectorStatusInfos() {
        return connectorStatusInfos;
    }

    @JsonProperty("ConnectorStatusInfos")
    public void setConnectorStatusInfos(List<ConnectorStatusInfo> connectorStatusInfos) {
        this.connectorStatusInfos = connectorStatusInfos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("stationID", stationID).append("connectorStatusInfos", connectorStatusInfos).append("additionalProperties", additionalProperties).toString();
    }

}