package com.ev49.evcs.dto;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.server.exception.ServerInternalException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class DTOJsonHelper {

    public static <T> T parseResponseData(String responseBody, Class<T> clz, String jsonListKey) {
        if (responseBody != null) {
            try {
                CommonResponse commonResponse = JSONUtil.readParams(responseBody, CommonResponse.class);
                return (T) commonResponse.anyDataType(clz, jsonListKey);
            } catch (Exception e) {
                String msg = e.getMessage();
                log.error(msg);
                throw new ServerInternalException(msg);
            }
        }
        return null;
    }

    public static <T> T parseResponseData(String responseBody, Class<T> clz) {
        if (responseBody != null) {
            try {
                CommonResponse commonResponse = JSONUtil.readParams(responseBody, CommonResponse.class);
                return JSONUtil.readParams((String) commonResponse.getData(), clz);
            } catch (Exception e) {
                String msg = e.getMessage();
                log.error(msg);
                throw new ServerInternalException(msg);
            }
        }
        return null;
    }

    public static <T> List<T> parseResponseDataAsList(String responseBody, Class<T> clz, String jsonListKey) {
        if (responseBody != null) {
            try {
                CommonResponse commonResponse = JSONUtil.readParams(responseBody, CommonResponse.class);
                return (List<T>) commonResponse.anyDataTypeList(clz, jsonListKey);
            } catch (Exception e) {
                String msg = e.getMessage();
                log.error(msg);
                throw new ServerInternalException(msg);
            }
        }
        return null;
    }

    public static <T> List<T> parseResponseDataAsList(String responseBody, Class<T> clz) {
        if (responseBody != null) {
            try {
                CommonResponse commonResponse = JSONUtil.readParams(responseBody, CommonResponse.class);
                return (List<T>) commonResponse.anyDataTypeList(clz);
            } catch (Exception e) {
                String msg = e.getMessage();
                log.error(msg);
                throw new ServerInternalException(msg);
            }
        }
        return null;
    }
}
