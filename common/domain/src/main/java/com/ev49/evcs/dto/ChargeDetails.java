package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChargeDetails {

    @JsonProperty("DetailStartTime")
    private String detailStartTime;

    @JsonProperty("DetailEndTime")
    private String detailEndTime;

    @JsonProperty("ElecPrice")
    private Double elecPrice = 0.0;

    @JsonProperty("SevicePrice")
    private Double sevicePrice = 0.0;

    @JsonProperty("DetailPower")
    private Double detailPower = 0.0;

    @JsonProperty("DetailElecMoney")
    private Double detailElecMoney = 0.0;

    @JsonProperty("DetailSeviceMoney")
    private Double detailSeviceMoney = 0.0;

}