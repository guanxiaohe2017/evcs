package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CheckOrderSeq",
        "StartTime",
        "EndTime",
        "TotalDisputeOrder",
        "TotalDisputePower",
        "TotalDisputeMoney",
        "DisputeOrders"
})
public class CheckChargeOrderResponseData {

    @JsonProperty("CheckOrderSeq")
    private String checkOrderSeq;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("EndTime")
    private String endTime;
    @JsonProperty("TotalDisputeOrder")
    private Integer totalDisputeOrder;
    @JsonProperty("TotalDisputePower")
    private Double totalDisputePower;
    @JsonProperty("TotalDisputeMoney")
    private Double totalDisputeMoney;
    @JsonProperty("DisputeOrders")
    private DisputeOrder[] disputeOrders = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CheckOrderSeq")
    public String getCheckOrderSeq() {
        return checkOrderSeq;
    }

    @JsonProperty("CheckOrderSeq")
    public void setCheckOrderSeq(String checkOrderSeq) {
        this.checkOrderSeq = checkOrderSeq;
    }

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("EndTime")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("EndTime")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("TotalDisputeOrder")
    public Integer getTotalDisputeOrder() {
        return totalDisputeOrder;
    }

    @JsonProperty("TotalDisputeOrder")
    public void setTotalDisputeOrder(Integer totalDisputeOrder) {
        this.totalDisputeOrder = totalDisputeOrder;
    }

    @JsonProperty("TotalDisputePower")
    public Double getTotalDisputePower() {
        return totalDisputePower;
    }

    @JsonProperty("TotalDisputePower")
    public void setTotalDisputePower(Double totalDisputePower) {
        this.totalDisputePower = totalDisputePower;
    }

    @JsonProperty("TotalDisputeMoney")
    public Double getTotalDisputeMoney() {
        return totalDisputeMoney;
    }

    @JsonProperty("TotalDisputeMoney")
    public void setTotalDisputeMoney(Double totalDisputeMoney) {
        this.totalDisputeMoney = totalDisputeMoney;
    }

    @JsonProperty("DisputeOrders")
    public DisputeOrder[] getDisputeOrders() {
        return disputeOrders;
    }

    @JsonProperty("DisputeOrders")
    public void setDisputeOrders(DisputeOrder[] disputeOrders) {
        this.disputeOrders = disputeOrders;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("checkOrderSeq", checkOrderSeq).append("startTime", startTime).append("endTime", endTime).append("totalDisputeOrder", totalDisputeOrder).append("totalDisputePower", totalDisputePower).append("totalDisputeMoney", totalDisputeMoney).append("disputeOrders", disputeOrders).append("additionalProperties", additionalProperties).toString();
    }

}