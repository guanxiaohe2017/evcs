package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeq",
        "StartChargeSeqStat",
        "ConnectorID",
        "SuccStat",
        "FailReason"
})
public class NotificationStopChargeResultRequestData {

    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonProperty("StartChargeSeqStat")
    private Integer startChargeSeqStat;
    @JsonProperty("ConnectorID")
    private String connectorId;
    @JsonProperty("SuccStat")
    private Integer succStat;
    @JsonProperty("FailReason")
    private Integer failReason;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonProperty("SuccStat")
    public Integer getSuccStat() {
        return succStat;
    }

    @JsonProperty("SuccStat")
    public void setSuccStat(Integer succStat) {
        this.succStat = succStat;
    }

    @JsonProperty("StartChargeSeqStat")
    public Integer getStartChargeSeqStat() {
        return startChargeSeqStat;
    }

    @JsonProperty("StartChargeSeqStat")
    public void setStartChargeSeqStat(Integer startChargeSeqStat) {
        this.startChargeSeqStat = startChargeSeqStat;
    }

    @JsonProperty("ConnectorID")
    public String getConnectorId() {
        return connectorId;
    }

    @JsonProperty("ConnectorID")
    public void setConnectorId(String connectorId) {
        this.connectorId = connectorId;
    }

    @JsonProperty("FailReason")
    public Integer getFailReason() {
        return failReason;
    }

    @JsonProperty("FailReason")
    public void setFailReason(Integer failReason) {
        this.failReason = failReason;
    }


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeq", startChargeSeq).append("succStat", succStat).append("startChargeSeqStat", startChargeSeqStat).append("connectorId", connectorId).append("additionalProperties", additionalProperties).toString();
    }

}
