package com.ev49.evcs.domain;

import com.ev49.evcs.dto.ChargeDetails;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;

@JsonPropertyOrder({
        "StartChargeSeq",
        "StartChargeSeqStat",
        "ConnectorID",
        "ConnectorStatus",
        "CurrentA",
        "CurrentB",
        "CurrentC",
        "VoltageA",
        "VoltageB",
        "VoltageC",
        "Soc",
        "StartTime",
        "ChargeModel",
        "EndTime",
        "TotalPower",
        "ElecMoney",
        "SeviceMoney",
        "TotalMoney",
        "SumPeriod",
        "ChargeDetails"
})

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Setter
@Getter
public class EquipChargeStatusInfo {

    @Id
    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;

    @JsonProperty("StartChargeSeqStat")
    private Integer startChargeSeqStat;

    @JsonProperty("ConnectorID")
    private String connectorId;

    @JsonProperty("ConnectorStatus")
    private Integer connectorStatus;

    @JsonProperty("CurrentA")
    private Double currentA = 15.0;

    @JsonProperty("CurrentB")
    private Double currentB = 15.0;

    @JsonProperty("CurrentC")
    private Double currentC = 15.0;

    @JsonProperty("VoltageA")
    private Double voltageA = 220.0;

    @JsonProperty("VoltageB")
    private Double voltageB = 220.0;

    @JsonProperty("VoltageC")
    private Double voltageC = 220.0;

    @JsonProperty("Soc")
    private Double soc = 0.0;

    @JsonProperty("StartTime")
    private String startTime;

    @JsonProperty("ChargeModel")
    private Integer chargeModel = 2;

    @JsonProperty("EndTime")
    private String endTime;

    @JsonProperty("TotalPower")
    private Double totalPower = 0.00;

    @JsonProperty("ElecMoney")
    private Double elecMoney = 0.00;

    @JsonProperty("SeviceMoney")
    private Double seviceMoney = 0.00;

    @JsonProperty("TotalMoney")
    private Double totalMoney = 0.00;

    @JsonProperty("SumPeriod")
    private Integer sumPeriod;

    @JsonProperty("ChargeDetails")
    private ChargeDetails chargeDetails[];

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("startChargeSeq", startChargeSeq)
                .append("startChargeSeqStat", startChargeSeqStat)
                .append("connectorId", connectorId)
                .append("connectorStatus", connectorStatus)
                .append("currentA", currentA)
                .append("currentB", currentB)
                .append("currentC", currentC)
                .append("voltageA", voltageA)
                .append("voltageB", voltageB)
                .append("voltageC", voltageC)
                .append("soc", soc)
                .append("startTime", startTime)
                .append("chargeModel", chargeModel)
                .append("endTime", endTime)
                .append("totalPower", totalPower)
                .append("elecMoney", elecMoney)
                .append("seviceMoney", seviceMoney)
                .append("totalMoney", totalMoney)
                .append("sumPeriod", sumPeriod)
                .append("chargeDetails", chargeDetails)
                .toString();
    }
}
