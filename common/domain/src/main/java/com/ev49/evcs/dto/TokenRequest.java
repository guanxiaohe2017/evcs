package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "OperatorID",
        "OperatorSecret"
})
public class TokenRequest {

    @JsonProperty("OperatorID")
    private String operatorID;
    @JsonProperty("OperatorSecret")
    private String operatorSecret;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("OperatorID")
    public String getOperatorID() {
        return operatorID;
    }

    @JsonProperty("OperatorID")
    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    @JsonProperty("OperatorSecret")
    public String getOperatorSecret() {
        return operatorSecret;
    }

    @JsonProperty("OperatorSecret")
    public void setOperatorSecret(String operatorSecret) {
        this.operatorSecret = operatorSecret;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("operatorID", operatorID).append("operatorSecret", operatorSecret).append("additionalProperties", additionalProperties).toString();
    }

}