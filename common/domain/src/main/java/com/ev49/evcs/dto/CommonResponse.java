package com.ev49.evcs.dto;

import com.ev49.common.utils.json.JSONUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.IOException;

@Getter@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class CommonResponse {

    public static final int SUCCESS = 0;
    @Id
    @GeneratedValue
    @JsonIgnore()
    @JsonProperty("Id")
    Long id;
    @JsonProperty("Ret")
    String ret;
    @JsonProperty("Data")
    @JsonRawValue
    Object data;
    @JsonProperty("Msg")
    String msg = "";
    @JsonProperty("Sig")
    String sig;

    public Object anyDataTypeList(Class clz, String key) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree((String) data);
        jsonNode = jsonNode.path(key);
        return JSONUtil.readParamsList(jsonNode.toString(), clz);
    }

    public Object anyDataTypeList(Class clz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree((String) data);
        return JSONUtil.readParamsList(jsonNode.toString(), clz);
    }

    public Object anyDataType(Class clz, String key) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree((String) data);
        jsonNode = jsonNode.path(key);
        return JSONUtil.readParams(jsonNode.toString(), clz);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Data", this.data).append("Msg", this.msg).append("Ret", this.ret).toString();
    }
}
