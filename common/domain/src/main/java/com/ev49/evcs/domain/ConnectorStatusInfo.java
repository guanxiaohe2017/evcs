package com.ev49.evcs.domain;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;


@JsonPropertyOrder({
        "ConnectorID",
        "Status"
})
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Getter
@Setter
public class ConnectorStatusInfo {

    final static int OFF_LINE = 0;//离网
    final static int FREE = 1;//空闲
    final static int CONNECTED = 2;//占用（未充电)
    final static int CHARGING = 3;//占用（充电中）
    final static int BOOKED = 4;//占用（预约锁定)
    final static int ERROR = 255;//故障

    @Id
    @JsonProperty("ConnectorID")
    private String connectorID;
    @JsonProperty("Status")
    private Integer status;
    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("connectorID", connectorID).append("status", status).append("additionalProperties", additionalProperties).toString();
    }

}
