package com.ev49.evcs.repository;

import com.ev49.evcs.domain.OperatorInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RestResource(path = "OperatorInfoes", rel = "OperatorInfo")
public interface OperatorInfoRepository extends JpaRepository<OperatorInfo, String>, QueryByExampleExecutor<OperatorInfo>, JpaSpecificationExecutor<OperatorInfo> {

    List<OperatorInfo> findByOperatorRegAddressLike(String host);

    List<OperatorInfo> findByOperatorID(String operatorID);
}
