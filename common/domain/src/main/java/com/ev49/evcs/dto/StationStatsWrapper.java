package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StationStatsInfo"
})
public class StationStatsWrapper {

    @JsonProperty("StationStats")
    private StationStatsInfo stationStatsInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StationStats")
    public StationStatsInfo getStationStatsInfo() {
        return stationStatsInfo;
    }

    @JsonProperty("StationStats")
    public void setStationStatsInfo(StationStatsInfo stationStatsInfo) {
        this.stationStatsInfo = stationStatsInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}