package com.ev49.evcs.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class CommonOperatorInfo {

    @Id
    @JsonProperty("OperatorID")
    String operatorID;
    @JsonProperty("OperatorName")
    String operatorName;
    @JsonProperty("OperatorTel1")
    String operatorTel1;
    @JsonProperty("OperatorTel2")
    String operatorTel2;
    @JsonProperty("OperatorRegAddress")
    String operatorRegAddress;
    @JsonProperty("OperatorNote")
    String operatorNote;

}
