package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "StartChargeSeq",
        "TotalPower ",
        "TotalMoney",
        "DisputeReason"
})
public class DisputeOrder {

    @JsonProperty("StartChargeSeq")
    private String startChargeSeq;
    @JsonProperty("TotalPower ")
    private Double totalPower;
    @JsonProperty("TotalMoney")
    private Double totalMoney;
    @JsonProperty("DisputeReason")
    private Integer disputeReason;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StartChargeSeq")
    public String getStartChargeSeq() {
        return startChargeSeq;
    }

    @JsonProperty("StartChargeSeq")
    public void setStartChargeSeq(String startChargeSeq) {
        this.startChargeSeq = startChargeSeq;
    }

    @JsonProperty("TotalPower")
    public Double getTotalPower() {
        return totalPower;
    }

    @JsonProperty("TotalPower")
    public void setTotalPower(Double totalPower) {
        this.totalPower = totalPower;
    }

    @JsonProperty("TotalMoney")
    public Double getTotalMoney() {
        return totalMoney;
    }

    @JsonProperty("TotalMoney")
    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    @JsonProperty("DisputeReason")
    public Integer getDisputeReason() {
        return disputeReason;
    }

    @JsonProperty("DisputeReason")
    public void setDisputeReason(Integer disputeReason) {
        this.disputeReason = disputeReason;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startChargeSeq", startChargeSeq).append("totalPower", totalPower).append("totalMoney", totalMoney).append("disputeReason", disputeReason).append("additionalProperties", additionalProperties).toString();
    }

}
