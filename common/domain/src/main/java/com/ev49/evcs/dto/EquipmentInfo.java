package com.ev49.evcs.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "EquipmentID",
        "ManufacturerID",
        "EquipmentType",
        "Power",
        "ConnectorInfos",
        "EquipmentLng",
        "EquipmentLat"
})
@Getter
@Setter
public class EquipmentInfo {

    @JsonProperty("EquipmentID")
    public String equipmentID;
    @JsonProperty("ManufacturerID")
    public String manufacturerID;
    @JsonProperty("EquipmentType")
    public Long equipmentType;
    @JsonProperty("Power")
    public Double power;
    @JsonProperty("ConnectorInfos")
    public List<ConnectorInfo> connectorInfos = null;
    @JsonProperty("EquipmentLng")
    public Double equipmentLng;
    @JsonProperty("EquipmentLat")
    public Double equipmentLat;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("equipmentID", equipmentID).append("manufacturerID", manufacturerID).append("equipmentType", equipmentType).append("power", power).append("connectorInfos", connectorInfos).append("equipmentLng", equipmentLng).append("equipmentLat", equipmentLat).append("additionalProperties", additionalProperties).toString();
    }

}