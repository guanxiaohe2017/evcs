package com.ev49.common.utils.encryption;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class Aes128Cbc {
    private static int AES_128 = 128;

    public static String encrypt(String msg, String rawkeyStr) throws NoSuchAlgorithmException, UnsupportedEncodingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException {

        KeyGenerator keyGenerator = KeyGenerator.getInstance(CryptoMngr.ALGORITHM);
        keyGenerator.init(AES_128);
        byte[] rawkey = (rawkeyStr).getBytes("UTF-8");
        rawkey = Arrays.copyOf(rawkey, 16);
        SecretKey key = new SecretKeySpec(rawkey, "AES");
        SecretKey IV = key;
        byte[] cipherText = CryptoMngr.encrypt(key.getEncoded(), IV.getEncoded(), msg.getBytes());
        return Base64.getEncoder().encodeToString(cipherText);
    }

    public static byte[] decryptBytes(String encryptedMsg, String rawkeyStr) throws NoSuchAlgorithmException, UnsupportedEncodingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException {

        KeyGenerator keyGenerator = KeyGenerator.getInstance(CryptoMngr.ALGORITHM);
        keyGenerator.init(AES_128);
        byte[] rawkey = (rawkeyStr).getBytes("UTF-8");
        rawkey = Arrays.copyOf(rawkey, 16);
        SecretKey key = new SecretKeySpec(rawkey, "AES");
        SecretKey IV = key;
        byte[] cipherText = Base64.getDecoder().decode(encryptedMsg.getBytes());
        byte[] decryptedBytes = CryptoMngr.decrypt(key.getEncoded(), IV.getEncoded(), cipherText);
        return decryptedBytes;
    }

    public static String decryptString(String encryptedMsg, String rawkeyStr) throws NoSuchAlgorithmException, UnsupportedEncodingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException {

        byte[] decryptedBytes = decryptBytes(encryptedMsg, rawkeyStr);
        return new String(decryptedBytes);
    }

    public static void main(String[] args) throws BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException {
        System.out.printf(decryptString("il7B0BSEjFdzpyKzfOFpvg/Se1CP802RItKYFPfSLRxJ3jf0bVl9hvYOEktPAYW2nd7S8MBcyHYyacHKbISq5iTmDzG+ivnR+SZJv3USNTYVMz9rCQVSxd0cLlqsJauko79NnwQJbzDTyLooYoIwz75qBOH2/xOMirpeEqRJrF/EQjWekJmGk9RtboXePu2rka+Xm51syBPhiXJAq0GfbfaFu9tNqs/e2Vjja/ltE1M0lqvxfXQ6da6HrThsm5id4ClZFIi0acRfrsPLRixS/IQYtksxghvJwbqOsbIsITail9Ayy4tKcogeEZiOO+4Ed264NSKmk7l3wKwJLAFjCFogBx8GE3OBz4pqcAn/ydA=", "1234567890abcdef"));
    }
}
