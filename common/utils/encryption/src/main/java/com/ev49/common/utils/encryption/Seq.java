package com.ev49.common.utils.encryption;

import org.joda.time.DateTime;

import java.util.Calendar;

public class Seq {

    private static Integer seq = null;

    public synchronized static String getNextNIncrease() {
        Calendar calendar = Calendar.getInstance();
        DateTime dateTime = new DateTime(calendar);
        int millisOfSecond = dateTime.getMillisOfSecond();
        String seqStr;
        if (seq == null || millisOfSecond == 0) {//第一次获取和新秒重记
            seq = millisOfSecond;
            seqStr = String.format("%04d", seq);
        } else {
            seq++;
            seqStr = String.format("%04d", seq);
            if (seq == 9999) {
                seq = null; //满9999重记
            }
        }
        return seqStr;
    }
}