package com.ev49.evcs.server.notification;

import com.ev49.evcs.domain.AuthSecretToken;
import com.ev49.evcs.repository.AuthSecretTokenRepository;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import javax.annotation.Resource;

public class AuthSecretTokenSecretTest extends BaseBootTest {

    @Resource
    private AuthSecretTokenRepository authSecretTokenRepository;

    @Test
    @Rollback(false)
    public void save() {
        AuthSecretToken authSecretToken = new AuthSecretToken();
        authSecretTokenRepository.save(authSecretToken);
    }

}
