package com.ev49.evcs.server.notification;

import com.ev49.evcs.server.notification.config.EvcsFilter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EvcsNotificationApplication.class)
@WebAppConfiguration
public class BaseWebappTest {

    @Autowired
    WebApplicationContext spring;
    @Autowired
    EvcsFilter evcsFilter;

    MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(spring).addFilter(evcsFilter).build();
    }

}
