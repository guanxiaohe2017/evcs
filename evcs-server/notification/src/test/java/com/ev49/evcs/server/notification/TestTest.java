package com.ev49.evcs.server.notification;

import com.ev49.evcs.domain.OperatorInfo;
import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.repository.OperatorInfoRepository;
import com.ev49.evcs.repository.StationInfoRepository;
import com.ev49.evcs.server.notification.config.EvcsFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EvcsNotificationApplication.class)
@WebAppConfiguration
public class TestTest {

	@Autowired WebApplicationContext spring;
	MockMvc mockMvc;
	@Autowired
    EvcsFilter evcsFilter;
	@Autowired
	StationInfoRepository stationInfoRepository;
	@Autowired
	private OperatorInfoRepository operatorInfoRepository;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(spring).addFilter(evcsFilter).build();

		List<StationInfo> stations = new ArrayList<>();
		StationInfo station1 = new StationInfo();
		station1.setStationName("Nanhu");
		station1.setEquipmentOwnerID("xxxxxxxxxxx");
		station1.setOperatorID("xx");
		station1.setStationID("zzzzzzzzzzz");
		station1.setCountryCode("CN");
		stations.add(station1);
		stationInfoRepository.saveAll(stations);
		List<OperatorInfo> oops = new ArrayList<>();
		OperatorInfo op1 = new OperatorInfo();
		op1.setOperatorID("xx");
		oops.add(op1);
		operatorInfoRepository.saveAll(oops);
	}
	
	@Test
	public void postStationInfos() throws Exception {

		mockMvc.perform(post("/v1/notification_stationStatus")
                .content("{\"OperatorID\":\"xx\",\"Data\":\"11F5ry/2y93X0Dg86V2SgMBHNkGZrez4Jmw0AvbaMzxkUJRYyWppZ74SMZoZsyyo\"}")
                .header("Authorization", "Bearer xxx")
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Ret").value("0"))
                .andExpect(jsonPath("$.Data").value("qg39mu5YIdvsOzUl8n3x0UXRZoVYoQFeNx0VMXhgOSY="));
	}

}
