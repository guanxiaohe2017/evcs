package com.ev49.evcs.server.notification;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class QueryAuthSecretTokenTest extends BaseWebappTest {

    @Test
    @Rollback(false)
    public void queryToken() throws Exception {

        mockMvc.perform(post("/v1/query_token").servletPath("/v1/query_token")
                .content("{\"OperatorID\":\"MA61XH!T69\",\"Data\":\"YCEPiEqLxkNWV179NcT0MC5FRWjMUZ46ps3JzlDGvzFqeuZhGZN82HuyYbO4xZTYwI9BSNTCxJ2Hd9LS5+C2FQ==\"}")
                .header("Content-Type", "application/json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Ret").value("0"));
    }

}
