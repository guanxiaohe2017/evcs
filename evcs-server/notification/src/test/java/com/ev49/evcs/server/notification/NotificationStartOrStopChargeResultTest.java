package com.ev49.evcs.server.notification;

import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NotificationStartOrStopChargeResultTest extends BaseWebappTest {

    @Test
    public void notifyStartChargeResult() throws Exception {

        mockMvc.perform(post("/v1/notification_start_charge_result")
                .content("{\"OperatorID\":\"MA61XHT69\",\"Data\":\"txMrDfBisaaKVMtgyjeN5IoBTV8mUNBkSLcO52hvNTNmQFbDSUJDEig2jUInhGo5GG4pIfNWUyyPuUmX2RYz8kI5Ob637JDRF9cvAqE+Xm7mR7QIkBGY06jKTpKUcTWua28KEKPKeO5mIPaL3yCI8swZYt7EL3TjAp2vrR+xnueuyvhiKeV/YNF4jXvAMaO9+hWTB5/AdZYiFTtc0nj9CV0YFhFj/y3lbqV9CCvNIfjpISuSCBUOemripYKS9U5W\"}")
                .header("Authorization", "Bearer 55c4443687b0cf0209a5a0025025f977")
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Ret").value("0"))
                .andExpect(jsonPath("$.Data").value("ZmeK2XdeagNPsSzCQtqZfi+O0g9XUvZVveEdG3swBkkhvEZaMNv7u+wC0VvhWh51l9/JzTG4soTmr9KMZZyQWzB1mlJN+XKbAjJwyoHXjXZrfpJjM0i8QEzPPdnVJYwF"));
    }

    @Test
    public void notifyStopChargeResult() throws Exception {

        mockMvc.perform(post("/v1/notification_stop_charge_result")
                .content("{\"OperatorID\":\"MA61XHT69\",\"Data\":\"y4MAW3YF9ZltQsdZoQwpXADAv3YfYtJtJFa9yO6kFcJ1It1x3VyjsBqr3ihDYBnDy3g0WSDCoEMpQ0gm2xnw8/2wwDzoa3Gaq++d/44b1sFnYb+pkzmjoNJhlMHZGarPU4GWvNtCX1hkyGCMAj1E1xMKf5jq1WFbh/zoD5M0zoCcsBilXuPyqGMzBCCt6tzok7CJUHvOXHMK/CyMqtHlJIPve+vW8VCWFnNjs1Ij9xkjsd9K3uQ9Dg8fsLbauvg4\"}")
                .header("Authorization", "Bearer 55c4443687b0cf0209a5a0025025f977")
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Ret").value("0"))
                .andExpect(jsonPath("$.Data").value("ZmeK2XdeagNPsSzCQtqZfi+O0g9XUvZVveEdG3swBkkhvEZaMNv7u+wC0VvhWh51l9/JzTG4soTmr9KMZZyQWxwR9a0YesNjDJ7ruKWVdnetNOYEWYT422VgrSu3txqE"));
    }
}
