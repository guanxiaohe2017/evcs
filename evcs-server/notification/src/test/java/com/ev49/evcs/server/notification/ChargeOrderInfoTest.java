package com.ev49.evcs.server.notification;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ChargeOrderInfoTest extends BaseWebappTest {

    @Test
    @Rollback(false)
    public void notifyChargeOrdersInfo() throws Exception {

        mockMvc.perform(post("/v1/notification_charge_order_info")
                .content("{\"OperatorID\":\"MA61XHT69\",\"Data\":\"11F5ry/2y93X0Dg86V2SgMBHNkGZrez4Jmw0AvbaMzxkUJRYyWppZ74SMZoZsyyo\"}")
                .header("Authorization", "Bearer 55c4443687b0cf0209a5a0025025f977")
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Ret").value("0"))
                .andExpect(jsonPath("$.Data").value("6ndDOmSpOmIG78dXYz25G+4OiP1pjxdkRkyuelYGCcHFEafHL3e6crY+7IbdKTRsRciJafJVQ3ZlI/qHUASp/A=="));
    }

}
