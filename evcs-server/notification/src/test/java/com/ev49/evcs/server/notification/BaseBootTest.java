package com.ev49.evcs.server.notification;

import com.ev49.evcs.server.notification.config.EvcsFilter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EvcsNotificationApplication.class)
public class BaseBootTest {

    @Autowired
    EvcsFilter evcsFilter;

    @Before
    public void setup() {

    }

}
