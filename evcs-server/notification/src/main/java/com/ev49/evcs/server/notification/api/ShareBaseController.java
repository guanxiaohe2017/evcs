package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.OperatorInfo;
import com.ev49.evcs.repository.OperatorInfoRepository;
import com.ev49.evcs.server.exception.ForbiddenException;
import com.ev49.evcs.server.exception.ServerInternalException;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ShareBaseController {

    @Resource
    OperatorInfoRepository operatorInfoRepository;

    private String SrvUrl;
    //这里应该使用四九平台IP
    @Value("${ev49.evshare.srv.url}")
    private String shareSrvUrl;
    @Value("${ev49.evcs.srv.url}")
    private String evcsSrvUrl;
    //@Value("${ev49.evcs.okhttp.log}")
    private boolean oklog = false;

    public static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");

    public OperatorInfo getOpertaor(String operatorId) {
        return operatorInfoRepository.findById(operatorId).orElseThrow(() -> new EntityNotFoundException("OperatorInfo::OperatorID[" + operatorId + "] not found."));
    }



    public String ok(Object object, String url, String type) {

        if ("evcs".equals(type)) {
            SrvUrl = evcsSrvUrl;
        } else if ("evshare".equals(type)){
            SrvUrl = shareSrvUrl;
        }
        okhttp3.RequestBody body;
        try {
            body = okhttp3.RequestBody.create(JSON, JSONUtil.toJSONString(object));
        } catch (JsonProcessingException e) {
            String msg = e.getMessage();
            log.error(msg);
            throw new ServerInternalException(msg);
        }
        Request request = new Request.Builder()
                .url(SrvUrl + url)
                .post(body)
                .build();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        Proxy proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved("127.0.0.1", 8888));
//        builder.proxy(proxy);
        //builder.addInterceptor(evcsInterceptor);
        if (oklog) {
            builder.addInterceptor(logging);
        }
        builder.connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return fwdToInfra(request, client);
    }

    private String fwdToInfra(Request request, OkHttpClient client) {
        String responseBody = null;
        try {
            Response response = client.newCall(request).execute();
            if (response != null && response.isSuccessful()) {
                responseBody = response.body().string();
            } else {
                int code = response.code();
                if (code == 401 || code == 403) {
                    throw new ForbiddenException("Remote system returned " + code);
                }
            }
        } catch (IOException e) {
            String msg = e.getMessage();
            log.error(msg);
            throw new ServerInternalException(msg);
        }
        return responseBody;
    }
}
