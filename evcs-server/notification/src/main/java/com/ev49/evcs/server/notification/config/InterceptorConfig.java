package com.ev49.evcs.server.notification.config;

import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@Configuration //using filter instead todo
public class InterceptorConfig implements WebMvcConfigurer {
 
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getEvcsInterceptor()).addPathPatterns("/v1/**");
    }

    @Bean
    public HandlerInterceptor getEvcsInterceptor() {
        return new EvcsInterceptor();
    }

}
