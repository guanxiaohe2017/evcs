package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.constants.EvcsConst;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController()
public class StartChargeResultController extends BaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;

	@PostMapping("/v1/notification_start_charge_result")
    public CommonResponse notifyStartChargeResult(@RequestBody CommonRequest<NotificationStartChargeResultRequestData> commonRequest) throws IOException {

		getOpertaor(commonRequest.getOperatorID());
		String data = commonRequest.getData();
		NotificationStartChargeResultRequestData startChargeResultRequest = JSONUtil.readParams(data, NotificationStartChargeResultRequestData.class);
		String startChargeSeq = startChargeResultRequest.getStartChargeSeq();
		ChargeOrderInfo chargeOrderInfo = chargeOrderInfoRepository.findById(startChargeSeq).orElse(new ChargeOrderInfo().setStartChargeSeqAndReturn(startChargeSeq));
		chargeOrderInfo.setStartChargeSeqStat(startChargeResultRequest.getStartChargeSeqStat());
		chargeOrderInfo.setIdentCode(startChargeResultRequest.getIdentCode());
		chargeOrderInfoRepository.save(chargeOrderInfo);
		CommonResponse response = new CommonResponse();
		response.setRet(EvcsConst.RET_SUCC);
		NotificationStartStopChargeResultResponse resp = new NotificationStartStopChargeResultResponse();
		resp.setSuccStat(0);
		resp.setFailReason(0);
		resp.setStartChargeSeq(startChargeResultRequest.getStartChargeSeq());
		response.setData(JSONUtil.toJSONString(resp));
		return response;
	}

}
