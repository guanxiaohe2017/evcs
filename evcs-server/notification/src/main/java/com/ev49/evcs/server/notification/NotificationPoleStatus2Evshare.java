package com.ev49.evcs.server.notification;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.repository.ConnectorStatusInfoRepository;
import com.ev49.evcs.server.notification.api.ShareBaseController;
import com.ev49.evcs.server.notification.utils.ChangePoleStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;

@Component
public class NotificationPoleStatus2Evshare extends ShareBaseController {

    @Autowired
    private ConnectorStatusInfoRepository connectorStatusInfoRepository;

    @Scheduled(fixedRate = 1000*60*1)
    private void notificationConnectorStatus() throws JsonProcessingException {
        List<ConnectorStatusInfo> connectorStatusInfos = connectorStatusInfoRepository.findAll();
        List<ConnectorStatusInfo> changeStatus = ChangePoleStatus.getChangeStatus(connectorStatusInfos);
        List<PoleStatusInfo> poleStatusInfos = new ArrayList<>();
        //用于过滤掉集合里重复的充电桩编号
        Map<String,Integer> infosMap = new HashMap<>();
        for (ConnectorStatusInfo statusInfo : changeStatus) {
            String connectorNo = statusInfo.getConnectorID();
            String poleNo = connectorNo.substring(0,connectorNo.length()-2);   //截掉

            if (statusInfo.getStatus() == 255 || statusInfo.getStatus() == 0) {
                infosMap.put(poleNo, 1);
            } else if (statusInfo.getStatus() == 1) {
                infosMap.put(poleNo, 0);
            } else {
                infosMap.put(poleNo, 2);
            }
        }

        Set<String> keys = infosMap.keySet();
        for (String key : keys) {
            PoleStatusInfo poleStatusInfo = new PoleStatusInfo();
            poleStatusInfo.setPoleNo(key);
            poleStatusInfo.setStatus(infosMap.get(key));
            poleStatusInfos.add(poleStatusInfo);
        }

        if (poleStatusInfos.size() > 0) {
            notificationPoleStatus2Evshare(poleStatusInfos);
        }
    }



    public String notificationPoleStatus2Evshare(@RequestBody List<PoleStatusInfo> poleStatusInfos) throws JsonProcessingException {
        String data = JSONUtil.toJSONString(poleStatusInfos);
        CommonRequest<List<PoleStatusInfo>> commonRequest = new CommonRequest<>();
        commonRequest.setOperatorID("MA61XHT69");
        commonRequest.setData(data);
        String responseBody = ok(commonRequest, "pole/notification_pole_status","evshare");
        return responseBody;
    }

}
