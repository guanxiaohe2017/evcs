package com.ev49.evcs.server.notification.config;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.common.utils.servlet.HttpServletRequestRepeatReadWrapper;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.repository.OperatorInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Scanner;

@SuppressWarnings({"Duplicates"})
@Slf4j
public class EvcsInterceptor implements HandlerInterceptor {

    @Resource
    OperatorInfoRepository operatorInfoRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {

        HttpServletRequest requestWrapper = new HttpServletRequestRepeatReadWrapper(request);
        Scanner scanner = new Scanner(requestWrapper.getInputStream(), "UTF-8").useDelimiter("\\A");
        String bodyString = scanner.hasNext() ? scanner.next() : null;
        if (!StringUtils.isEmpty(bodyString)) {
            CommonRequest commonRequest = JSONUtil.readParams(bodyString, CommonRequest.class);
            if (commonRequest != null) {
                String operatorID = commonRequest.getOperatorID();
                operatorInfoRepository.findById(operatorID).orElseThrow(() -> new EntityNotFoundException("OperatorInfo::OperatorID[" + operatorID + "] not found."));
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    }

}
