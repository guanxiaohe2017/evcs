package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.constants.EvcsConst;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.ChargeOrderInfoResponse;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController()
public class ChargeOrderInfoController extends BaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;

    @PostMapping("/v1/notification_charge_order_info")
    public CommonResponse notificationChargeOrderInfo(@RequestBody CommonRequest<ChargeOrderInfo> commonRequest) throws IOException {

        String operatorID = commonRequest.getOperatorID();
        getOpertaor(operatorID);
        StationInfo stationInfo = new StationInfo();
        stationInfo.setOperatorID(operatorID);
        String data = commonRequest.getData();
        ChargeOrderInfo chargeOrderInfo = JSONUtil.readParams(data, ChargeOrderInfo.class);
        chargeOrderInfo.setInfraOperatorId("759588065"); //todo use common request operator id?
        chargeOrderInfo.setBillerOperatorId("MA61XHT69");
        log.info(">>notify charge order OID: " + operatorID);
        chargeOrderInfoRepository.save(chargeOrderInfo);
        CommonResponse response = new CommonResponse();
        response.setRet(EvcsConst.RET_SUCC);
        ChargeOrderInfoResponse resp = new ChargeOrderInfoResponse();
        resp.setConfirmResult(0);
        resp.setConnectorID(chargeOrderInfo.getConnectorID());
        resp.setStartChargeSeq(chargeOrderInfo.getStartChargeSeq());
        response.setData(JSONUtil.toJSONString(resp));
        return response;
    }
}
