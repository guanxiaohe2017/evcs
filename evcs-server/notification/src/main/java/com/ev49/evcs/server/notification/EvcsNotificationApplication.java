package com.ev49.evcs.server.notification;

import com.ev49.evcs.server.notification.config.EvcsFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;

@EntityScan("com.ev49")
@ComponentScan("com.ev49")
@EnableJpaRepositories("com.ev49")
@SpringBootApplication
@EnableScheduling
public class EvcsNotificationApplication {


    public static void main(String[] args) {
        SpringApplication.run(EvcsNotificationApplication.class, args);
    }

    @PostConstruct
    public void init() {
    }

    @Bean //duplicated to @Component annotation, make doFilter invoke twice, nah... gone?
    public Filter getEvcsFilter() {
        System.out.println(">>>> bean filter init <<<<");
        return new EvcsFilter();
    }

}
