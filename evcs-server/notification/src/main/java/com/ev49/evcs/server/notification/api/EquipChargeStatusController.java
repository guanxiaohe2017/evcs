package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.constants.EvcsConst;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import com.ev49.evcs.repository.ConnectorStatusInfoRepository;
import com.ev49.evcs.server.exception.ServerInternalException;
import com.ev49.evcs.server.notification.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;

@RestController()
public class EquipChargeStatusController extends ShareBaseController {

    @Autowired
    ChargeOrderInfoRepository chargeOrderInfoRepository;

    @PostMapping("/v1/query_equip_charge_status")
    public CommonResponse QueryEquipChargeStatus(@RequestBody CommonRequest<ChargeInfoRequest> commonRequest) throws IOException {
        EquipChargeStatusCDResponse cdResponse = new EquipChargeStatusCDResponse();
        String data = commonRequest.getData();
        ChargeInfoRequest chargeInfoRequest = JSONUtil.readParams(data, ChargeInfoRequest.class);
        String responseBody = "";
        String startChargeSeq = chargeInfoRequest.getStartChargeSeq();
        //这里要做一下转换 给成都市那边的订单号是 运营商ID（9位） + 填0（4位） + 四九订单号 填充够27位
        if ("0000".equals(startChargeSeq.substring(9,13))) {
            startChargeSeq = startChargeSeq.substring(13);
            chargeInfoRequest.setStartChargeSeq(startChargeSeq);
        }
        if (startChargeSeq.length() > 14) {
            ChargeOrderInfo chargeOrderInfo = chargeOrderInfoRepository.findById(startChargeSeq).orElse(null);
            if (chargeOrderInfo == null) {
                throw new ServerInternalException("未查询到该订单编号数据");
            }
            cdResponse.setStartChargeSeq(chargeOrderInfo.getStartChargeSeq());
            cdResponse.setStartChargeSeqStat(chargeOrderInfo.getStartChargeSeqStat());
            cdResponse.setConnectorID(chargeOrderInfo.getConnectorID());
            cdResponse.setStartTime(chargeOrderInfo.getStartTime());
            String endTime;
            if (chargeOrderInfo.getEndTime() != null) {
                endTime = chargeOrderInfo.getEndTime();
            } else {
                endTime = DateUtil.date2String(new Date(),"yyyy-MM-dd HH:mm:ss");
            }
            cdResponse.setEndTime(endTime);
            cdResponse.setTotalPower(chargeOrderInfo.getTotalPower());
            cdResponse.setElecMoney(chargeOrderInfo.getTotalElecMoney());
            cdResponse.setSeviceMoney(chargeOrderInfo.getTotalSeviceMoney());
            cdResponse.setTotalMoney(chargeOrderInfo.getTotalMoney());

            ChargeDetails detail = new ChargeDetails();
            detail.setDetailStartTime(cdResponse.getStartTime());
            detail.setDetailEndTime(endTime);
            detail.setDetailSeviceMoney(cdResponse.getSeviceMoney());
            detail.setDetailElecMoney(cdResponse.getElecMoney());
            detail.setDetailPower(cdResponse.getTotalPower());
            ChargeDetails[] details = {detail};
            cdResponse.setChargeDetails(details);

        } else {
            responseBody = ok(chargeInfoRequest,"equip/query_equip_charge_status","evshare");
            cdResponse = JSONUtil.readParams(responseBody, EquipChargeStatusCDResponse.class);
            if (cdResponse.getStartChargeSeq() == null || "".equals(cdResponse.getStartChargeSeq())) {
                throw new ServerInternalException("未查询到该订单编号数据");
            } else {
                String seq = cdResponse.getStartChargeSeq();
                cdResponse.setStartChargeSeq("MA61XHT690000"+seq);
            }
        }

        CommonResponse response = new CommonResponse();
        response.setRet(EvcsConst.RET_SUCC);
        response.setData(JSONUtil.toJSONString(cdResponse));
        return response;
    }
}
