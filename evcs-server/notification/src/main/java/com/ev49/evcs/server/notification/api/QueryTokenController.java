package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.AuthSecretToken;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.dto.TokenRequest;
import com.ev49.evcs.dto.TokenResponse;
import com.ev49.evcs.repository.AuthSecretTokenRepository;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Slf4j
@RestController()
public class QueryTokenController extends BaseController {

    @Autowired
    private AuthSecretTokenRepository authSecretTokenRepository;

    @PostMapping("/v1/query_token")
    public CommonResponse queryToken(@RequestBody TokenRequest tokenRequest) throws IOException {

        log.info("<<query token request body: " + tokenRequest);
        CommonResponse resp = new CommonResponse();
        resp.setRet("0");
        resp.setMsg("");
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setFailReason(0);
        String operatorID = tokenRequest.getOperatorID();
        getOpertaor(operatorID);
        AuthSecretToken authSecretToken = getAuthSecretToken(operatorID);
        tokenResponse.setOperatorID(operatorID);
        tokenResponse.setSuccStat(0);
        tokenResponse.setTokenAvailableTime(7200);
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        tokenResponse.setAccessToken(token);
        authSecretToken.setToken(token);
        DateTime dt = new DateTime();
        dt = dt.plusSeconds(7200);
        Date tokenExpiry = dt.toDate();
        authSecretToken.setTokenExpiry(tokenExpiry);
        authSecretTokenRepository.save(authSecretToken);
        if ("759588065".equals(operatorID)) {
            authSecretToken = getAuthSecretToken("MA61XHT69");
            authSecretToken.setToken(token);
            authSecretToken.setTokenExpiry(tokenExpiry);
            authSecretTokenRepository.save(authSecretToken);
        }
        resp.setData(JSONUtil.toJSONString(tokenResponse));
        return resp;
    }

    private AuthSecretToken getAuthSecretToken(String operatorID) {
        return authSecretTokenRepository.findByOperatorIdAndSecretTokenType(operatorID, AuthSecretToken.SECRET_TOKEN_TYPE_IN).orElse(new AuthSecretToken(operatorID, AuthSecretToken.SECRET_TOKEN_TYPE_IN));
    }

}
