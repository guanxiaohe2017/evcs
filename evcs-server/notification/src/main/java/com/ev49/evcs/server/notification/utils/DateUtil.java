package com.ev49.evcs.server.notification.utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static final String DEFAULT_DATE_FORMAT = "yyyyMMddHHmmss";

    public static final String DATE_FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    
    public static final String DATE_FORMAT_MONTH_TIME = "MM-dd HH:mm";
  
    public static final String YEAR_MONTH_DAY = "yyyy-MM-dd";

    /**
     * convert string(such as: 2003-11-26) to Date
     * 
     * @param dateStr
     *            string format of date
     * @return
     */
    public static final Date string2Date(String dateStr) {
        return string2Date(dateStr + " 00:00:00", DATE_FORMAT_DATE_TIME);
    }

    /**
     * string to date, the format is same to SimpleDateFormat for example
     * "yyyyMMdd" "yyyy-MM-dd HH:mm:ss" etc please see
     * java.text.SimpleDateFormat
     * 
     * @param dateStr
     * @param format
     * @return
     */
    public static final Date string2Date(String dateStr, String format) {
        if (dateStr == null || dateStr.length() == 0)
            return null;
        DateFormat df = new SimpleDateFormat(format);
        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * 将long型字符串转换成时间格式
     * @param dateTime  
     * @return
     */
    public static final String longStr2Date(String dateTime){
    	 if (dateTime == null || dateTime.length() == 0)
             return null;
    	 Date date = new Date(Long.parseLong(dateTime));
    	 
         return date2String(date, DATE_FORMAT_DATE_TIME);
    }
    
    /**
     * convert date to string according to default format
     * 
     * @param date
     * @return
     */
    public static final String date2String(Date date) {
        return date2String(date, DEFAULT_DATE_FORMAT);
    }

    /**
     * convert date to string, and the format is same to SimpleDateFormat for
     * example "yyyyMMdd" "yyyy-MM-dd HH:mm" etc please see
     * java.text.SimpleDateFormat
     * 
     * @param date
     * @param format
     * @return
     */
    public static final String date2String(Date date, String format) {
        if (date == null) {
            return "";
        }
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }


    /**
     * date 2006-04-10 author :zhaopeng
     * 
     */
    private static SimpleDateFormat sf = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    public static Date getWeek(Date date) {

        Calendar c = getCalendar(date);

//        int m = c.get(Calendar.DAY_OF_WEEK);
//        if (m - 1 == 0) {
//            c.add(Calendar.DAY_OF_WEEK, -6);
//        } else {
//            c.add(Calendar.DAY_OF_WEEK, -(m - 2));
//        }
        c.add(Calendar.DAY_OF_WEEK, -Calendar.DAY_OF_WEEK);
        sf.format(c.getTime());
        return c.getTime();

    }

    public static Date getMonth(Date date) {
        Calendar c = getCalendar(date);
        int m = c.get(Calendar.DAY_OF_MONTH);

        c.add(Calendar.DAY_OF_MONTH, -(m - 1));
        return c.getTime();
    }

    public static void getMailConsole() {

    }

    public static Date getMonth(Date date, int num) {
        Calendar c = getCalendar(date);
        c.add(Calendar.MONTH, -num);
        return c.getTime();
    }

    public static Date getYear(Date date) {
        Calendar c = getCalendar(date);
        c.add(Calendar.YEAR, -1);
        return c.getTime();
    }

    /**
     * add by cjh convert date to string, and the format is same to
     * SimpleDateFormat for example "yyyyMMdd" "yyyy-MM-dd HH:mm" etc please see
     * java.text.SimpleDateFormat
     * 
     * @param date
     * @param format
     * @return
     */
    public static final String date2ChineseString(Date date) {
        if (date == null) {
            return "";
        }
        DateFormat df = new SimpleDateFormat("yyyy年M月d日");
        String str = df.format(date);
        int yearBorder = str.indexOf("年");
        int monthBorder = str.indexOf("月");
        int dayBorder = str.indexOf("日");
        return getChineseDate(str.substring(0, 1))
                + getChineseDate(str.substring(1, 2))
                + getChineseDate(str.substring(2, 3))
                + getChineseDate(str.substring(3, 4)) + '年'
                + getChineseDate(str.substring(yearBorder + 1, monthBorder))
                + '月'
                + getChineseDate(str.substring(monthBorder + 1, dayBorder))
                + '日';
    }

    /**
     * add by cjh ��������
     * 
     * @param date
     * @return
     */
    public static String getChineseDate(String date) {
        StringBuffer sb = new StringBuffer();
        if (date.length() == 2) {
            String ten = date.substring(0, 1); // ʮλ
            String entries = date.substring(1, 2); // ��λ
            if (ten.substring(0).equals("1")) {
                sb.append("ʮ");
            }
            if (ten.substring(0).equals("2")) {
                sb.append("��ʮ");
            }
            if (ten.substring(0).equals("3")) {
                sb.append("��ʮ");
            }
            if (!entries.equals("0")) {
                sb.append(number2Chinese(entries));
            }
        } else {
            String entries = date.substring(0);
            sb.append(number2Chinese(entries));
        }
        return sb.toString();
    }

    /**
     * add by cjh ����ת����
     * 
     * @param number
     * @return
     */
    public static String number2Chinese(String number) {
        char[] numberChars = number.toCharArray();
        switch (numberChars[0]) {
        case '0':
            return "零";
        case '1':
            return "一";
        case '2':
            return "二";
        case '3':
            return "三";
        case '4':
            return "四";
        case '5':
            return "五";
        case '6':
            return "六";
        case '7':
            return "七";
        case '8':
            return "八";
        case '9':
            return "九";
        default:
            return "";
        }
    }

    /**
     * Returns natural days between beginDate and endDate. Positive number if
     * beginDate before c2, negative if beginDate after endDate, 0 if beginDate
     * and endDate represent the same day.
     * 
     * @param beginDate
     *            the begin date
     * @param endDate
     *            the end date
     * @return natural days between begin date and end date
     */
    
    public static BigDecimal dateToExcel(Date date) {
    	Date endDate = DateUtil.string2Date("1900-01-00 00:00","yyyy-MM-dd HH:mm");
    	int days = naturalDaysBetween(endDate, date);
    	
    	return new BigDecimal(days).add(timeToExcel(date));
    }
    
    public static BigDecimal timeToExcel(Date date) {
    	Calendar c1 = getCalendar(date);
    	
    	int hour = c1.get(Calendar.HOUR_OF_DAY);
    	int min = c1.get(Calendar.MINUTE);
    	int sec = c1.get(Calendar.SECOND);
    	System.out.print(new BigDecimal(hour * 3600 + min * 60 + sec).divide(new BigDecimal(86400),10, BigDecimal.ROUND_HALF_UP).setScale(10, BigDecimal.ROUND_HALF_UP));
    	return new BigDecimal(hour * 3600 + min * 60 + sec).divide(new BigDecimal(86400),10, BigDecimal.ROUND_HALF_UP).setScale(10, BigDecimal.ROUND_HALF_UP);
    }
    
    
    
    
    public static int naturalDaysBetween(Date beginDate, Date endDate) {
        long msPerDay = 1000 * 60 * 60 * 24;
        Calendar c1 = getCalendar(beginDate);
        Calendar c2 = getCalendar(endDate);
        long msDiff = c2.getTimeInMillis() - c1.getTimeInMillis();
        int days = (int) (msDiff / msPerDay);
        int msResidue = (int) (msDiff % msPerDay);
        Calendar c3 = Calendar.getInstance();
        c3.setTimeInMillis(c2.getTimeInMillis() - msResidue);
        Calendar c4 = (Calendar) c2.clone();
        c4.add(Calendar.DAY_OF_MONTH, -1);
        if (c3.get(Calendar.DAY_OF_MONTH) == c4.get(Calendar.DAY_OF_MONTH))
            days++;
        else {
            c4.add(Calendar.DAY_OF_MONTH, 2);
            if (c3.get(Calendar.DAY_OF_MONTH) == c4.get(Calendar.DAY_OF_MONTH))
                days--;
        }
        return days;
    }

    /**
     * Returns natural days,hours and minutes between beginDate and endDate.
     * 
     * @author johnny 2007-1-31
     * @param beginDate
     *            the begin date
     * @param endDate
     *            the end date
     * @return int[]:[0]:days;[1]:hours;[2]:minutes
     */
    
    
    public static int[] naturalDHMBetween(Date beginDate, Date endDate) {
        int[] dhm = new int[3];
        if (beginDate == null || endDate == null)
            return dhm;
        long intervalSecond, leftSecond;
        intervalSecond = leftSecond = (endDate.getTime() - beginDate.getTime()) / 1000;
        dhm[0] = (int) intervalSecond / (60 * 60 * 24);
        leftSecond = leftSecond - dhm[0] * 60 * 60 * 24;
        dhm[1] = (int) leftSecond / (60 * 60);
        leftSecond = leftSecond - dhm[1] * 60 * 60;
        dhm[2] = (int) leftSecond / 60;
        return dhm;
    }

    public static Calendar getCalendar(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static Date earliestOfDate(Date date) {
        Calendar c = getCalendar(date);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public static Date latestOfDate(Date date) {
        Calendar c = getCalendar(date);
        c.set(Calendar.HOUR, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static int getHourOfDate(Date date) {
        Calendar c = getCalendar(date);
        return c.get(Calendar.HOUR);
    }

    public static boolean isFirstDayOfMonth(Date date) {
        Calendar c = getCalendar(date);
        return c.get(Calendar.DATE) == 1;
    }

    /**
     * Ϊָ���������ָ����ݡ��·ݺ�������µ�����
     */
    public static Date addDate(Date date, int year, int month, int day) {
        Calendar c = getCalendar(date);
        c.add(Calendar.YEAR, year);
        c.add(Calendar.MONTH, month);
        c.add(Calendar.DATE, day);
        return c.getTime();
    }

    /**
     * Ϊָ���������ָ��Сʱ�����ӡ���ͺ��룬�����µ�����
     */
    public static Date addTime(Date date, int hour, int minute, int second,
            int millisecond) {
        Calendar c = getCalendar(date);
        c.add(Calendar.HOUR_OF_DAY, hour);
        c.add(Calendar.MINUTE, minute);
        c.add(Calendar.SECOND, second);
        c.add(Calendar.MILLISECOND, millisecond);
        return c.getTime();
    }

    public static Date addDay(Date date, int days) {
        Calendar c = getCalendar(date);
        c.add(Calendar.DATE, days);
        return c.getTime();
    }

    public static Date getPreviousDate(Date date) {
        return addDay(date, -1);
    }

    public static boolean isTheSameDay(Date date1, Date date2) {
        String value1 = date2String(date1, DEFAULT_DATE_FORMAT);
        String value2 = date2String(date2, DEFAULT_DATE_FORMAT);
        return value1 != null && value1.equals(value2);
    }
    
    /**
     * 判断是否为周末
     * @param date
     * @return
     */
    public static boolean isWeekend(Date date) {
    	Calendar c = getCalendar(date);
    	int day = c.get(Calendar.DAY_OF_WEEK);
    	
    	return day == 1 || day == 7;
    }
    
    public static String week(Date date) {
    	Calendar c = getCalendar(date);
    	int day = c.get(Calendar.DAY_OF_WEEK);
    	
    	String week = "";
    	switch (day) {
	    	case 1 : week = "周日"; break;
	    	
	    	case 2 : week = "周一"; break;
	    	
	    	case 3 : week = "周二"; break;
	    	
	    	case 4 : week = "周三"; break;
	    	
	    	case 5 : week = "周四"; break;
	    	
	    	case 6 : week = "周五"; break;
	    	
	    	case 7 : week = "周六"; break;
    	}
    	
    	return week;
    }
    
    public static String getNowDateStr(){
    	
    	Calendar now = Calendar.getInstance();
		int year=now.get(Calendar.YEAR);
		int month=now.get(Calendar.MONTH) + 1;
		int day=now.get(Calendar.DAY_OF_MONTH);
		String dayNames[] = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五","星期六" };
		int dayOfWeek = now.get(Calendar.DAY_OF_WEEK)-1;
		if(dayOfWeek<0)dayOfWeek=0;
		String week=dayNames[dayOfWeek];
		String nowDateStr=year+"年"+month+"月"+day+"日 "+week;
		
		return nowDateStr;
    	
    }
    
    public static void main(String[] args) {
		DateUtil.isWeekend(new Date());
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-08", "yyyy-MM-dd"));
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-09", "yyyy-MM-dd"));
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-10", "yyyy-MM-dd"));
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-11", "yyyy-MM-dd"));
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-12", "yyyy-MM-dd"));
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-13", "yyyy-MM-dd"));
		DateUtil.isWeekend(DateUtil.string2Date("2016-06-14", "yyyy-MM-dd"));
	}
}