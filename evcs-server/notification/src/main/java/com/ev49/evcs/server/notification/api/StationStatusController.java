package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.constants.EvcsConst;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.dto.Status;
import com.ev49.evcs.repository.ConnectorStatusInfoRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

@RestController()
public class StationStatusController extends BaseController {

    @Resource
    ConnectorStatusInfoRepository connectorStatusInfoRepository;

    @PostMapping("/v1/notification_stationStatus")
    public CommonResponse notificationStationStatus(@RequestBody CommonRequest<ConnectorStatusInfo> commonRequest) throws IOException {
        String operatorID = commonRequest.getOperatorID();
        getOpertaor(operatorID);
        StationInfo stationInfo = new StationInfo();
        stationInfo.setOperatorID(operatorID);
        ConnectorStatusInfo connectorStatusInfo = commonRequest.anyDataType(ConnectorStatusInfo.class, "ConnectorStatusInfo");
        connectorStatusInfoRepository.save(connectorStatusInfo);
        CommonResponse response = new CommonResponse();
        response.setRet(EvcsConst.RET_SUCC);
        response.setData(JSONUtil.toJSONString(new Status(0)));
        return response;
    }
}
