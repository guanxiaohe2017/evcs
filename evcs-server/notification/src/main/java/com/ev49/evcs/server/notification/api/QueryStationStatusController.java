package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.dto.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QueryStationStatusController extends BaseController {
    @PostMapping("/v1/query_station_status")
    public CommonResponse queryStationsInfo(@RequestBody CommonRequest<StationStatusRequest> commonRequest) throws Exception {
        StationStatusRequest stationStatusRequest = JSONUtil.readParams(commonRequest.getData(), StationStatusRequest.class);
        String stationIDs[] = stationStatusRequest.getStationIds();
        String data = ok(stationIDs, "/weixin/queryStationStatus");
        List<StationStatusInfo> stationStatusInfo = JSONUtil.readParamsList(data, StationStatusInfo.class);
        StationStatusInfoWrapper stationStatusInfoWrappers = new StationStatusInfoWrapper();
        stationStatusInfoWrappers.setTotal(stationStatusInfo.size());
        stationStatusInfoWrappers.setStationStatusInfos(stationStatusInfo);
        CommonResponse resp = new CommonResponse();
        resp.setRet("0");
        resp.setMsg("");
        resp.setData(JSONUtil.toJSONString(stationStatusInfoWrappers));
        return resp;
    }
}
