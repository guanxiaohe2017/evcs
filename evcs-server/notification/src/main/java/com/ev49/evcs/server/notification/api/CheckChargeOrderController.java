package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.constants.EvcsConst;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController()
public class CheckChargeOrderController extends BaseController {

    @PostMapping("/v1/check_charge_orders")
    public CommonResponse notificationChargeOrderInfo(@RequestBody CommonRequest<CheckChargeOrderRequestData> commonRequest) throws IOException {

        String operatorID = commonRequest.getOperatorID();
        getOpertaor(operatorID);
        StationInfo stationInfo = new StationInfo();
        stationInfo.setOperatorID(operatorID);
        String data = commonRequest.getData();
        CheckChargeOrderRequestData req = JSONUtil.readParams(data, CheckChargeOrderRequestData.class);
        CommonResponse response = new CommonResponse();
        response.setRet(EvcsConst.RET_SUCC);
        CheckChargeOrderResponseData resp = new CheckChargeOrderResponseData();
        resp.setCheckOrderSeq(req.getCheckOrderSeq());
        resp.setStartTime(req.getStartTime());
        resp.setEndTime(req.getEndTime());
        resp.setDisputeOrders(new DisputeOrder[]{});
        resp.setTotalDisputeMoney(0.0);
        resp.setTotalDisputeOrder(0);
        resp.setTotalDisputePower(0.0);
        response.setData(JSONUtil.toJSONString(resp));
        return response;
    }
}
