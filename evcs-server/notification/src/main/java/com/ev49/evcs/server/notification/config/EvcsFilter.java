package com.ev49.evcs.server.notification.config;

import com.ev49.common.utils.encryption.Aes128Cbc;
import com.ev49.common.utils.encryption.HMAC;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.common.utils.servlet.HttpServletRequestRepeatReadWrapper;
import com.ev49.common.utils.servlet.HttpServletRequestWritableWrapper;
import com.ev49.evcs.domain.AuthSecretToken;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.repository.AuthSecretTokenRepository;
import com.ev49.evcs.repository.OperatorInfoRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.EntityNotFoundException;
import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Scanner;

@SuppressWarnings({"Duplicates"}) //todo
@Slf4j
//@Component //spring boot way, see https://www.surasint.com/spring-boot-webfilter-instead-of-component/
//@Order(1) not supported, see https://github.com/spring-projects/spring-boot/issues/8276
@WebFilter(urlPatterns = {"/v1/*", "/v2/*"}, filterName = "v1n2filter") //multiple filters execute by filterName order
public class EvcsFilter extends OncePerRequestFilter {

    @Resource
    private AuthSecretTokenRepository authSecretTokenRepository;
    @Resource
    private OperatorInfoRepository operatorInfoRepository;
    @Value("${ev49.evcs.encrypt:false}")
    private boolean encrypt = false;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        if (!encrypt) {
            chain.doFilter(request, response);
            return;
        }

        ServletRequest requestWrapper = new HttpServletRequestRepeatReadWrapper(request);
        Scanner scanner = new Scanner(requestWrapper.getInputStream(), "UTF-8").useDelimiter("\\A");
        String bodyString = scanner.hasNext() ? scanner.next() : null;
        log.debug("in: " + bodyString);

        if (!StringUtils.isEmpty(bodyString)) {
            String servletPath = request.getServletPath();
            log.debug("servletPath: " + servletPath);
            String secretKey;
            String operatorId;

            CommonRequest commonRequest = JSONUtil.readParams(bodyString, CommonRequest.class);
            operatorId = commonRequest.getOperatorID();

            operatorInfoRepository.findById(operatorId).orElseThrow(() -> new EntityNotFoundException("OperatorInfo::OperatorID[" + operatorId + "] not found."));
            AuthSecretToken authSecretToken = authSecretTokenRepository.findByOperatorIdAndSecretTokenType(operatorId, AuthSecretToken.SECRET_TOKEN_TYPE_IN).orElse(null);
            if (authSecretToken == null && servletPath.endsWith("/query_token")) {
                ((HttpServletRequestRepeatReadWrapper) requestWrapper).setBody(bodyString.getBytes());
                chain.doFilter(requestWrapper, response);
                return;
            }

            String authorization = request.getHeader("Authorization");
            if (authorization != null && authorization.startsWith("Bearer ")) {
                String token = authorization.substring(7);
                authSecretToken = authSecretTokenRepository.findByOperatorIdAndSecretTokenTypeAndTokenExpiryGreaterThan(operatorId, AuthSecretToken.SECRET_TOKEN_TYPE_IN, Calendar.getInstance().getTime()).orElse(null);
                if (authSecretToken == null || !token.equals(authSecretToken.getToken())) {
                    throw new EntityNotFoundException("Invalid token.");
                }
            } else {
                authSecretToken = authSecretTokenRepository.findByOperatorIdAndSecretTokenType(operatorId, AuthSecretToken.SECRET_TOKEN_TYPE_IN).orElse(null);
            }

            if (authSecretToken == null) {
                throw new EntityNotFoundException("Invalid token.");
            }
            secretKey = authSecretToken.getSecret();

            //decrypt request
            byte[] decryptedData = null;
            String erroMsg = null;
            try {
                decryptedData = decrypt(request, secretKey, commonRequest, bodyString);
            } catch (BadPaddingException e) {
                e.printStackTrace();
                erroMsg = e.getMessage();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
                erroMsg = e.getMessage();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                erroMsg = e.getMessage();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
                erroMsg = e.getMessage();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
                erroMsg = e.getMessage();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
                erroMsg = e.getMessage();
            }
            if (decryptedData != null) {
                requestWrapper = new HttpServletRequestWritableWrapper(request, decryptedData);
            } else {
                response.sendError(403, erroMsg);
            }

            //encrypt response
            ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);
            if (response.getHeader("enc") == null) {
                if (requestWrapper == null) {
                    chain.doFilter(request, responseWrapper);
                } else {
                    chain.doFilter(requestWrapper, responseWrapper);
                }
                byte[] buf = responseWrapper.getContentAsByteArray();
                String encryptedData = encrypt(secretKey, buf);
                response.getOutputStream().write(encryptedData == null ? internalError() : encryptedData.getBytes("UTF-8"));
                if (encryptedData != null) {
                    response.setHeader("enc", "true");
                }
            }
        }
    }

    private byte[] internalError() {
        CommonResponse resp = new CommonResponse();
        resp.setData("");
        resp.setRet("1");
        resp.setMsg("Internal error.");
        return resp.toString().getBytes();
    }

    private String encrypt(String key, byte[] buf) {
        String body;
        JsonNode rootNode;
        String data = null;
        if (buf.length > 0) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                rootNode = objectMapper.readTree(buf);
                JsonNode dataNode = rootNode.path("Data");
                String rawData = dataNode.toString();
                log.debug("out data: " + rawData);
                data = Aes128Cbc.encrypt(rawData, key);
                ((ObjectNode) rootNode).put("Data", data);
            } catch (Exception e) {
                rootNode = new ObjectNode(JsonNodeFactory.instance);
                ((ObjectNode) rootNode).put("Msg", e.getMessage());
            }
        } else {
            rootNode = new ObjectNode(JsonNodeFactory.instance);
        }
        String sig = HMAC.hmacDigest(rootNode.path("Ret").asText() + rootNode.path("Msg").asText() + data, key);
        ((ObjectNode) rootNode).put("Sig", sig);
        body = rootNode.toString();
        log.debug("out whole: " + body);
        return body;
    }

    private byte[] decrypt(HttpServletRequest request, String key, CommonRequest commonRequest, String bodyString) throws IOException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {

        byte[] buf = new byte[]{};
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            if (request.getServletPath().endsWith("/query_token")) {
                String data = Aes128Cbc.decryptString(commonRequest.getData(), key);
                commonRequest.setData(data);
                buf = data.getBytes("UTF-8");
            } else {
                String authorization = request.getHeader("Authorization");
                boolean auth = false;
                if (authorization != null && authorization.startsWith("Bearer ")) {
                    //auth
//                    String token = operatorInfo.getToken(); //todo shoulda gen&save AuthSecretToken while /v1/query_token being invoked
//                    if (authorization.substring(7).equals(token)) {
                    //decrypt Data field
                    buf = bodyString.getBytes("UTF-8");
                    ObjectMapper objectMapper = new ObjectMapper();
                    JsonNode rootNode = objectMapper.readTree(buf);
                    JsonNode dataNode = rootNode.path("Data");
                    if (!dataNode.isNull()) {
                        String rawData = dataNode.asText();
                        String decryptedData = Aes128Cbc.decryptString(rawData, key);
                        ((ObjectNode) rootNode).put("Data", decryptedData);
                        buf = rootNode.toString().getBytes();
                        auth = true;
                    }
//                    }
                }
                if (!auth) {
                    throw new AuthenticationException("Not authorized.");
                }
            }
        }
        return buf;
    }

    @Override
    public void destroy() {

    }

}