package com.ev49.evcs.server.notification.utils;

import com.ev49.evcs.domain.ConnectorStatusInfo;

import java.util.*;

/**
 * 该全局类用于缓存上一次定时的数据
 */
public class ChangePoleStatus {
    public static List<ConnectorStatusInfo> temp = new ArrayList<>();

    public static List<ConnectorStatusInfo> getChangeStatus(List<ConnectorStatusInfo> NewStatus){
        List<ConnectorStatusInfo> changeStatus = new ArrayList<>();
        ConnectorStatusInfo connectorStatusInfo = new ConnectorStatusInfo();
        if (temp.isEmpty()) {
            temp = NewStatus;
            return NewStatus;
        } else {
            Map<String, Integer> oldStatus = new HashMap<>();
            Map<String, Integer> newStatus = new HashMap<>();

            for (ConnectorStatusInfo info:NewStatus) {
                newStatus.put(info.getConnectorID(),info.getStatus());
            }

            for (ConnectorStatusInfo info:temp) {
                oldStatus.put(info.getConnectorID(),info.getStatus());
            }

            Set<String> setPre = oldStatus.keySet();
            Set<String> setNew = newStatus.keySet();
            for (String string : setNew) {
                if (!newStatus.get(string).equals(oldStatus.get(string))) {
                    connectorStatusInfo.setConnectorID(string);
                    connectorStatusInfo.setStatus(newStatus.get(string));
                    changeStatus.add(connectorStatusInfo);
                }
            }

            temp = NewStatus;
            return changeStatus;
        }
    }
}
