package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.dto.ChargeResultRequest;
import com.ev49.evcs.dto.ChargeResultResponse;
import lombok.extern.slf4j.Slf4j;
import com.ev49.common.utils.constants.EvcsConst;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.dto.NotificationStartStopChargeResultResponse;
import com.ev49.evcs.dto.NotificationStopChargeResultRequestData;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController()
public class StopChargeResultController extends BaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;

    @PostMapping("/v1/notification_stop_charge_result")
    public CommonResponse notifyStartChargeResult(@RequestBody CommonRequest<NotificationStopChargeResultRequestData> commonRequest) throws IOException {

        getOpertaor(commonRequest.getOperatorID());
        String data = commonRequest.getData();
        NotificationStopChargeResultRequestData stopChargeResultRequestData = JSONUtil.readParams(data, NotificationStopChargeResultRequestData.class);
        String startChargeSeq = stopChargeResultRequestData.getStartChargeSeq();
        ChargeOrderInfo chargeOrderInfo = chargeOrderInfoRepository.findById(startChargeSeq).orElse(new ChargeOrderInfo().setStartChargeSeqAndReturn(startChargeSeq));
        chargeOrderInfo.setStartChargeSeqStat(stopChargeResultRequestData.getStartChargeSeqStat());
        chargeOrderInfo.setFailReason(stopChargeResultRequestData.getFailReason());
        chargeOrderInfoRepository.save(chargeOrderInfo);
        CommonResponse response = new CommonResponse();
        response.setRet(EvcsConst.RET_SUCC);
        NotificationStartStopChargeResultResponse resp = new NotificationStartStopChargeResultResponse();
        resp.setSuccStat(0);
        resp.setFailReason(0);
        resp.setStartChargeSeq(stopChargeResultRequestData.getStartChargeSeq());
        response.setData(JSONUtil.toJSONString(resp));
        return response;
    }

}
