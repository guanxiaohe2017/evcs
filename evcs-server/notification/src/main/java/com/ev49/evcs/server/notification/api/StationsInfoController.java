package com.ev49.evcs.server.notification.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.dto.PageRequest;
import com.ev49.evcs.dto.PageStationsInfoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class StationsInfoController extends BaseController {

    @PostMapping("/v1/query_stations_info")
    public CommonResponse queryStationsInfo(@RequestBody CommonRequest<PageRequest> commonRequest) throws Exception {
        PageRequest pageRequest = JSONUtil.readParams(commonRequest.getData(), PageRequest.class);
        Map<String, Object> query = new HashMap<>();
        query.put("pageSize", pageRequest.getPageSize());
        query.put("pageStart", pageRequest.getPageNo());
        query.put("LastQueryTime", pageRequest.getLastQueryTime());
        String data = ok(query, "/weixin/findAllStations");
        PageStationsInfoResponse response = JSONUtil.readParams(data, PageStationsInfoResponse.class);
        CommonResponse resp = new CommonResponse();
        resp.setRet("0");
        resp.setMsg("");
        resp.setData(JSONUtil.toJSONString(response));
        return resp;
    }

}
