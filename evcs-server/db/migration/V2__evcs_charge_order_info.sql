CREATE TABLE `et_charge_order_info` (
	`id` INT(11) NOT NULL,
	`connectorid` VARCHAR(255) NULL DEFAULT NULL,
	`end_time` VARCHAR(255) NULL DEFAULT NULL,
	`start_charge_seq` VARCHAR(255) NULL DEFAULT NULL,
	`start_time` VARCHAR(255) NULL DEFAULT NULL,
	`stop_reason` INT(11) NULL DEFAULT NULL,
	`sum_period` INT(11) NULL DEFAULT NULL,
	`total_elec_money` DOUBLE NULL DEFAULT NULL,
	`total_money` DOUBLE NULL DEFAULT NULL,
	`total_power` DOUBLE NULL DEFAULT NULL,
	`total_sevice_money` DOUBLE NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
