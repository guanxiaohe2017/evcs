-- --------------------------------------------------------
-- 主机:                           192.168.255.49
-- 服务器版本:                        5.7.21 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 evcszztest.common_operator_info 结构
CREATE TABLE IF NOT EXISTS `common_operator_info` (
  `dtype` varchar(31) NOT NULL,
  `operatorid` varchar(255) NOT NULL,
  `operator_name` varchar(255) DEFAULT NULL,
  `operator_note` varchar(255) DEFAULT NULL,
  `operator_reg_address` varchar(255) DEFAULT NULL,
  `operator_tel1` varchar(255) DEFAULT NULL,
  `operator_tel2` varchar(255) DEFAULT NULL,
  `operator_secret` varchar(255) DEFAULT NULL,
  `raw_token` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expiry` datetime DEFAULT NULL,
  PRIMARY KEY (`operatorid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  evcszztest.common_operator_info 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `common_operator_info` DISABLE KEYS */;
INSERT INTO `common_operator_info` (`dtype`, `operatorid`, `operator_name`, `operator_note`, `operator_reg_address`, `operator_tel1`, `operator_tel2`, `operator_secret`, `raw_token`, `token`, `token_expiry`) VALUES
	('OperatorInfo', 'xx', NULL, NULL, NULL, NULL, NULL, '6B1E87D6310D835A', NULL, NULL, NULL);
/*!40000 ALTER TABLE `common_operator_info` ENABLE KEYS */;

-- 导出  表 evcszztest.common_station_info 结构
CREATE TABLE IF NOT EXISTS `common_station_info` (
  `dtype` varchar(31) NOT NULL,
  `stationid` varchar(255) NOT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `equipment_ownerid` varchar(255) DEFAULT NULL,
  `operatorid` varchar(255) DEFAULT NULL,
  `station_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  evcszztest.common_station_info 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `common_station_info` DISABLE KEYS */;
INSERT INTO `common_station_info` (`dtype`, `stationid`, `country_code`, `equipment_ownerid`, `operatorid`, `station_name`) VALUES
	('StationInfo', 'zzzzzzzzzzz', 'CN', 'xxxxxxxxxxx', 'xx', 'Nanhu');
/*!40000 ALTER TABLE `common_station_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
