package com.ev49.evcs.server.query;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.OperatorInfo;
import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.PageRequest;
import com.ev49.evcs.repository.OperatorInfoRepository;
import com.ev49.evcs.repository.StationInfoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EvcsQueryApplication.class)
@WebAppConfiguration
public class TestTest {

    @Autowired
    WebApplicationContext spring;
    MockMvc mockMvc;

    @Autowired
    StationInfoRepository stationInfoRepository;
    @Autowired
    private OperatorInfoRepository operatorInfoRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(spring).build();

        List<StationInfo> stations = new ArrayList<>();
        StationInfo station1 = new StationInfo();
        station1.setStationName("Nanhu");
        station1.setEquipmentOwnerID("xxxxxxxxxxx");
        station1.setOperatorID("xx");
        station1.setStationID("zzzzzzzzzzz");
        station1.setCountryCode("CN");
        stations.add(station1);
        stationInfoRepository.saveAll(stations);
        List<OperatorInfo> oops = new ArrayList<>();
        OperatorInfo op1 = new OperatorInfo();
        op1.setOperatorID("xx");
//        op1.setTokenExpiry(); //todo intro joda time
        oops.add(op1);
        operatorInfoRepository.saveAll(oops);
    }

    @Test
    public void queryStationInfos() throws Exception {

        mockMvc.perform(post("/v1/query_stations_info")
                .content("{}")
                .header("OperatorID", "xx")
                .header("Content-Type", "application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void queryOrderInfo() throws Exception {

        PageRequest pageRequest = new PageRequest();
        pageRequest.setPageNo(1);
        pageRequest.setPageSize(10);
        String request = JSONUtil.toJSONString(pageRequest);
        mockMvc.perform(post("/v1/query_order_infoByChargeSeq")
                .content("{\"Data\":\"{\\\"StartChargeSeq\\\":\\\"111\\\"}\"}")
                .header("Content-Type", "application/json"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void queryOrderInfosByBillerId() throws Exception {

        mockMvc.perform(post("/v1/query_order_info")
                .content("{\"QueryType\":1,\"BillerOperatorId\":\"123\"}")
                .header("Content-Type", "application/json"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void queryConnectorStatusInfo() throws Exception {

        mockMvc.perform(post("/v1/query_connector_status_info")
                .content("{\"Data\":\"{\\\"ConnectorID\\\":\\\"111\\\"}\"}")
                .header("Content-Type", "application/json"))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public void queryStationInfosTypeInferred() throws Exception {
        CommonRequest<PageRequest> commonReqDataPageReq = new CommonRequest<>();
        commonReqDataPageReq.setOperatorID("MA61XHT69");
        PageRequest pageReq = new PageRequest();
        commonReqDataPageReq.setData(JSONUtil.toJSONString(pageReq));
        mockMvc.perform(post("/v1/query_stations_info")
                .content(JSONUtil.toJSONString(commonReqDataPageReq))
                .header("OperatorID", "xx")
                .header("Content-Type", "application/json"))
                .andExpect(status().is5xxServerError());
    }
}
