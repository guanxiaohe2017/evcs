package com.ev49.evcs.server.query.api;

import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.StationStatsInfo;
import com.ev49.evcs.dto.StationStatsRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController()
public class StationStatsController extends BaseController {

    @PostMapping("/v1/query_station_stats")
    public StationStatsInfo queryStationStats(@RequestBody CommonRequest<StationStatsRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_station_stats");
        StationStatsInfo stationStatsInfo = DTOJsonHelper.parseResponseData(responseBody, StationStatsInfo.class);
        return stationStatsInfo;
    }

}
