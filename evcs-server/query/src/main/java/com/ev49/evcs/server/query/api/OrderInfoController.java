package com.ev49.evcs.server.query.api;


import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import com.ev49.evcs.server.exception.ServerInternalException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController()
public class OrderInfoController extends BaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;

    /**
     *
     * @param byBillerOperatorIdRequest
     * @param pageable
     * @return
     * @throws IOException
     */
    @PostMapping("/v1/query_order_info")
    public Page<ChargeOrderInfo> queryOrderInfo(@RequestBody ChargeInfoByBillerOperatorIdRequest byBillerOperatorIdRequest, Pageable pageable) throws IOException {

        String billerOperatorId = "";
        Page<ChargeOrderInfo> chargeOrderInfos = null;
        if (byBillerOperatorIdRequest.getQueryType() == 1) {
            billerOperatorId = byBillerOperatorIdRequest.getBillerOperatorId();
            chargeOrderInfos = chargeOrderInfoRepository.findChargeOrderInfosByBillerOperatorId(billerOperatorId,pageable);
        } else if (byBillerOperatorIdRequest.getQueryType() == 2) {
            chargeOrderInfos = chargeOrderInfoRepository.findChargeOrderInfosByBillerOperatorIdIsNull(pageable);
        }else {
            chargeOrderInfos = chargeOrderInfoRepository.findAll(pageable);
        }

        return  chargeOrderInfos;
    }

    /**
     * 根据订单序号 查询订单详细信息
     * @param commonRequest
     * @return
     * @throws IOException
     */
    /**
     * 返回对象集合
     * @param evShareQueryChargeInfosRequest
     * @param pageable
     * @return
     * @throws IOException
     */
    @PostMapping("/v1/query_order_info_by_biller")
    public List<ChargeOrderInfo> queryOrderInfoByBiller(@RequestBody EvShareQueryChargeInfosRequest evShareQueryChargeInfosRequest, Pageable pageable) throws IOException {

        String billerOperatorId = evShareQueryChargeInfosRequest.getBillerOperatorId();
        List<ChargeOrderInfo> chargeOrderInfos = null;
        String sTime = evShareQueryChargeInfosRequest.getStartTime();
        String eTime = evShareQueryChargeInfosRequest.getEndTime();
        if (eTime != null && !"".equals(eTime)) {
            eTime = eTime + " 23:59:59";
        }
        int type = evShareQueryChargeInfosRequest.getQueryType();
        Specification<ChargeOrderInfo> querySpecifi = getSpecification(sTime, eTime,billerOperatorId,type);

        chargeOrderInfos = chargeOrderInfoRepository.findAll(querySpecifi);

        return  chargeOrderInfos;
    }

    /**
     * 真分页方法
     * @param pagingRequest
     * @param
     * @return Page<ChargeOrderInfo>
     * @throws IOException
     */
    @PostMapping("/v1/query_order_info_by_biller_paging")
    public Page<ChargeOrderInfo> queryOrderInfoByBillerPaging(@RequestBody EvShareQueryChargeInfosPagingRequest pagingRequest) {

        Pageable pageable = PageRequest.of(pagingRequest.getPage(), pagingRequest.getPageSize(), Sort.by(Sort.Direction.DESC, "endTime"));

        String billerOperatorId = pagingRequest.getBillerOperatorId();
        String sTime = pagingRequest.getStartTime();
        String eTime = pagingRequest.getEndTime();
        if (eTime != null && !"".equals(eTime)) {
            eTime = eTime + " 23:59:59";
        }
        int type = pagingRequest.getQueryType();
        Specification<ChargeOrderInfo> querySpecifi = getSpecification(sTime, eTime,billerOperatorId,type);
        Page<ChargeOrderInfo> chargeOrderInfos = chargeOrderInfoRepository.findAll(querySpecifi, pageable);

        return  chargeOrderInfos;
    }

    /**
     * 获得统计数据 不用sum函数
     * @param evShareQueryChargeInfosRequest
     * @return
     * @throws IOException
     */
    @PostMapping("/v1/query_order_sum")
    public OrderSumInfo queryOrderSum(@RequestBody EvShareQueryChargeInfosRequest evShareQueryChargeInfosRequest) throws IOException {

        String billerOperatorId = evShareQueryChargeInfosRequest.getBillerOperatorId();
        List<ChargeOrderInfo> chargeOrderInfos = null;
        String sTime = evShareQueryChargeInfosRequest.getStartTime();
        String eTime = evShareQueryChargeInfosRequest.getEndTime();
        if (eTime != null && !"".equals(eTime)) {
            eTime = eTime + " 23:59:59";
        }
        int type = evShareQueryChargeInfosRequest.getQueryType();
        Specification<ChargeOrderInfo> querySpecifi = getSpecification(sTime, eTime,billerOperatorId,type);

        chargeOrderInfos = chargeOrderInfoRepository.findAll(querySpecifi);

        BigDecimal allElectric = new BigDecimal(0);
        BigDecimal allElectricMoney = new BigDecimal(0);
        BigDecimal allServiceMoney = new BigDecimal(0);
        BigDecimal allMoney = new BigDecimal(0);

        for (ChargeOrderInfo order : chargeOrderInfos) {
            allElectric = allElectric.add(BigDecimal.valueOf(order.getTotalPower()));
            allElectricMoney = allElectricMoney.add(BigDecimal.valueOf(order.getTotalElecMoney()));
            allServiceMoney = allServiceMoney.add(BigDecimal.valueOf(order.getTotalSeviceMoney()));
            allMoney = allMoney.add(BigDecimal.valueOf(order.getTotalMoney()));
        }

        OrderSumInfo orderSumInfo = new OrderSumInfo(allElectric,allElectricMoney,allServiceMoney
        ,allMoney);

        return  orderSumInfo;
    }

    /**
     * 获得统计数据 使用sum函数
     * @param evShareQueryChargeInfosRequest
     * @return
     * @throws IOException
     */
    @PostMapping("/v1/query_order_sum_by_sql")
    public OrderSumInfo queryOrderSumBySql(@RequestBody EvShareQueryChargeInfosRequest evShareQueryChargeInfosRequest) throws IOException,NumberFormatException {

        String billerOperatorId = evShareQueryChargeInfosRequest.getBillerOperatorId();
        List<ChargeOrderInfo> chargeOrderInfos = null;
        String sTime = evShareQueryChargeInfosRequest.getStartTime();
        String eTime = evShareQueryChargeInfosRequest.getEndTime();
        if (eTime != null && !"".equals(eTime)) {
            eTime = eTime + " 23:59:59";
        }
        int type = evShareQueryChargeInfosRequest.getQueryType();

        Double allElectric = 0.0;
        Double allElectricMoney = 0.0;
        Double allServiceMoney = 0.0;
        Double allMoney = 0.0;

        if (type == 1) {
            allElectric = chargeOrderInfoRepository.sumSJTotalPower(billerOperatorId, sTime, eTime);
            allElectricMoney = chargeOrderInfoRepository.sumSJTotalElecMoney(billerOperatorId, sTime, eTime);
            allServiceMoney = chargeOrderInfoRepository.sumSJTotalServiceMoney(billerOperatorId, sTime, eTime);
            allMoney = chargeOrderInfoRepository.sumSJTotalMoney(billerOperatorId, sTime, eTime);
        } else if (type == 2) {
            allElectric = chargeOrderInfoRepository.sumYDTotalPower(sTime, eTime);
            allElectricMoney = chargeOrderInfoRepository.sumYDTotalElecMoney(sTime, eTime);
            allServiceMoney = chargeOrderInfoRepository.sumYDTotalServiceMoney(sTime, eTime);
            allMoney = chargeOrderInfoRepository.sumYDTotalMoney(sTime, eTime);
        } else {
            allElectric = chargeOrderInfoRepository.sumAllTotalPower(sTime, eTime);
            allElectricMoney = chargeOrderInfoRepository.sumAllTotalElecMoney(sTime, eTime);
            allServiceMoney = chargeOrderInfoRepository.sumAllTotalServiceMoney(sTime, eTime);
            allMoney = chargeOrderInfoRepository.sumAllTotalMoney(sTime, eTime);
        }

        if (allElectric == null || allElectricMoney == null  || allElectricMoney == null  || allElectricMoney == null) {
            throw new ServerInternalException("查询出的统计数据为null");
        }

        BigDecimal bigAllElectric = new BigDecimal(allElectric);
        BigDecimal bigAllElectricMoney = new BigDecimal(allElectricMoney);
        BigDecimal bigAllServiceMoney = new BigDecimal(allServiceMoney);
        BigDecimal bigAllMoney = new BigDecimal(allMoney);

        OrderSumInfo orderSumInfo = new OrderSumInfo(bigAllElectric,bigAllElectricMoney,bigAllServiceMoney,bigAllMoney);

        return  orderSumInfo;
    }


    /**
     * 根据订单编号查询订单信息
     * @param commonRequest
     * @return
     * @throws IOException
     */
    @PostMapping("/v1/query_order_infoByChargeSeq")
    public ChargeOrderInfo queryOrderInfoById(@RequestBody CommonRequest<ChargeInfoRequest> commonRequest) throws IOException {

        ChargeInfoRequest data = JSONUtil.readParams(commonRequest.getData(),ChargeInfoRequest.class);
        if (data.getStartChargeSeq() == null) {
            throw new ServerInternalException("请传入订单编号参数");
        }
        String startChargeSeq = data.getStartChargeSeq();
        return chargeOrderInfoRepository.findById(startChargeSeq).orElse(null);
    }

    /**
     * 使用Specification定义查询过滤条件
     * @param sTime
     * @param eTime
     * @param billerOperatorId
     * @param type
     * @return
     */
    private Specification<ChargeOrderInfo> getSpecification(String sTime,String eTime,String billerOperatorId, int type){
        Specification<ChargeOrderInfo> querySpecifi = new Specification<ChargeOrderInfo>() {
            @Override
            public Predicate toPredicate(Root<ChargeOrderInfo> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();
                if (StringUtils.isNotBlank(sTime)) {
                    //大于或等于传入时间
                    predicates.add(cb.greaterThanOrEqualTo(root.get("endTime").as(String.class), sTime));
                }
                if (StringUtils.isNotBlank(eTime)) {
                    //小于或等于传入时间
                    predicates.add(cb.lessThanOrEqualTo(root.get("endTime").as(String.class), eTime));
                }

                if (StringUtils.isNotBlank(billerOperatorId) && type == 1) {

                    predicates.add(cb.like(root.get("billerOperatorId").as(String.class), billerOperatorId));
                }

                if (type == 2) {
                    predicates.add(cb.isNull(root.get("billerOperatorId")));
                   /* predicates.add(cb.like(root.get("billerOperatorId").as(String.class), billerOperatorId));*/
                }

                /*if (type == 0) {
                    //模糊查找
                    //predicates.add(cb.like(root.get("billerOperatorId").as(String.class), "%"+""+"%"));
                }*/
                // and到一起的话所有条件就是且关系，or就是或关系
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return querySpecifi;
    }

}
