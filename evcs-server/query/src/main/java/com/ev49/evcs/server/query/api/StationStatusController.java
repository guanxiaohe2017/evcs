package com.ev49.evcs.server.query.api;

import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.StationStatusInfo;
import com.ev49.evcs.dto.StationStatusRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController()
public class StationStatusController extends BaseController {

    @PostMapping("/v1/query_station_status")
    public List<StationStatusInfo> queryStationStatus(@RequestBody CommonRequest<StationStatusRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_station_status");
        List<StationStatusInfo> stationStatusInfos = DTOJsonHelper.parseResponseDataAsList(responseBody, StationStatusInfo.class, "StationStatusInfos");
        return stationStatusInfos;
    }

}
