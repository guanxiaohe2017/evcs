package com.ev49.evcs.server.query.api;

import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.dto.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController()
public class ChargeOrderInfoController extends BaseController {

    @PostMapping("/v1/notification_charge_order_info")
    public ChargeOrderInfoResponse notificationChargeOrderInfo(@RequestBody CommonRequest<ChargeOrderInfo> commonRequest) throws IOException {
        String responseBody =  ok(commonRequest,"notification_charge_order_info");

        ChargeOrderInfoResponse chargeOrderInfoResponse = DTOJsonHelper.parseResponseData(responseBody, ChargeOrderInfoResponse.class, "ChargeOrderInfoResponse");


        return chargeOrderInfoResponse;
    }
}