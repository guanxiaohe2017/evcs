package com.ev49.evcs.server.query.api;

import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.EquipBizRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController()
public class EquipBusinessPolicyController extends BaseController {

    @PostMapping("/v1/query_equip_business_policy")
    public List<StationInfo> queryEquipmentBusinessPolicy(@RequestBody CommonRequest<EquipBizRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_equip_business_policy");
        List<StationInfo> stationList = DTOJsonHelper.parseResponseDataAsList(responseBody, StationInfo.class, "StationInfos");
        return stationList;
    }

}
