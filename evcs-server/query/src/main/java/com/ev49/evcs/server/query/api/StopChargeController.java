package com.ev49.evcs.server.query.api;

import com.ev49.evcs.dto.ChargeRequest;
import com.ev49.evcs.dto.ChargeResponse;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController()
public class StopChargeController extends BaseController {

    @PostMapping("/v1/query_stop_charge")
    public ChargeResponse queryStopCharge(@RequestBody CommonRequest<ChargeRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_stop_charge");
        ChargeResponse chargeResponse = DTOJsonHelper.parseResponseData(responseBody, ChargeResponse.class);
        return chargeResponse;
    }

}
