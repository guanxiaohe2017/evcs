package com.ev49.evcs.server.query;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.repository.ConnectorStatusInfoRepository;
import com.ev49.evcs.server.exception.ServerInternalException;
import com.ev49.evcs.server.query.api.CDBaseController;
import com.ev49.evcs.server.query.utils.ChangePoleStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Component
public class NotificationConnectorStatusTask extends CDBaseController {

    @Autowired
    private ConnectorStatusInfoRepository connectorStatusInfoRepository;

    @Scheduled(cron = "0/10 * * * * ? ")
    private void notificationConnectorStatus() throws JsonProcessingException {
        List<ConnectorStatusInfo> connectorStatusInfos = connectorStatusInfoRepository.findAll();
        List<ConnectorStatusInfo> changeStatus = ChangePoleStatus.getChangeStatus(connectorStatusInfos);
        for (ConnectorStatusInfo statusInfo : changeStatus) {
            Status status = notificationStationStatus(statusInfo);
        }
    }


    public Status notificationStationStatus(@RequestBody ConnectorStatusInfo connectorStatusInfoRequest) throws JsonProcessingException {
        ConnectorStatusInfo2CDRequest statusInfo2CDRequest = new ConnectorStatusInfo2CDRequest();
        statusInfo2CDRequest.setConnectorStatusInfo(connectorStatusInfoRequest);
        String data = JSONUtil.toJSONString(statusInfo2CDRequest);
        CommonRequest<ConnectorStatusInfo2CDRequest> commonRequest = new CommonRequest<>();
        commonRequest.setOperatorID("MA61XHT69");
        commonRequest.setData(data);

        String responseBody = ok(commonRequest, "notification_stationStatus");

        Status status = DTOJsonHelper.parseResponseData(responseBody, Status.class);

        return status;
    }

}
