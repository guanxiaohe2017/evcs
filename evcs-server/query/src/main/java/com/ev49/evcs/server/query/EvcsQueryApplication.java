package com.ev49.evcs.server.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EntityScan(basePackages = {"com.ev49"})
@EnableJpaRepositories("com.ev49")
@EnableScheduling
public class EvcsQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvcsQueryApplication.class, args);
    }

    @PostConstruct
    public void init() {
    }

}
