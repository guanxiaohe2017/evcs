package com.ev49.evcs.server.query.api;

import com.ev49.evcs.server.query.ChargeOrderTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class ChargeOrderTaskController extends BaseController {

    @Autowired
    private ChargeOrderTask cot;

    @PostMapping("/runCOT")
    public String runChargeOrderTask(@RequestBody String start) throws JsonProcessingException {
        if (!StringUtils.isEmpty(start)) {
            cot.syncOrders(start);
        }
        return "ok";
    }

}
