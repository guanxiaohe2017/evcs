package com.ev49.evcs.server.query.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.EquipChargeStatusInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.CommonResponse;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.NotificationEquipChargeStatusData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@Slf4j
@RestController()
public class NotificationEquipChargeController extends CDBaseController{

    @PostMapping("/v1/evcs_notification_equip_charge_status")
    private NotificationEquipChargeStatusData equipCharge(@RequestBody CommonRequest<EquipChargeStatusInfo> commonRequest) throws IOException {
        String responseBody = ok(commonRequest, "notification_equip_charge_status");
        CommonResponse commonResponse = JSONUtil.readParams(responseBody,CommonResponse.class);
        NotificationEquipChargeStatusData statusData = new NotificationEquipChargeStatusData();
        if ("0".equals(commonResponse.getRet())) {
            return DTOJsonHelper.parseResponseData(commonResponse.getData().toString(), NotificationEquipChargeStatusData.class);
        } else {
            statusData.setSuccStat(1);
            return statusData;
        }
    }

}
