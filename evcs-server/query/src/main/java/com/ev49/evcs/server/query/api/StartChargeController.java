package com.ev49.evcs.server.query.api;

import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.dto.ChargeRequest;
import com.ev49.evcs.dto.ChargeResponse;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController()
public class StartChargeController extends BaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;

    @PostMapping("/v1/query_start_charge")
    public ChargeResponse queryStartCharge(@RequestBody CommonRequest<ChargeRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_start_charge");
        ChargeResponse chargeResponse = DTOJsonHelper.parseResponseData(responseBody, ChargeResponse.class);
        ChargeOrderInfo chargeOrderInfo = new ChargeOrderInfo();
        chargeOrderInfo.setStartChargeSeq(chargeResponse.getStartChargeSeq());
        chargeOrderInfo.setBillerOperatorId("MA61XHT69");
        chargeOrderInfo.setConnectorID(chargeResponse.getConnectorID());
        chargeOrderInfoRepository.save(chargeOrderInfo);
        return chargeResponse;
    }

}
