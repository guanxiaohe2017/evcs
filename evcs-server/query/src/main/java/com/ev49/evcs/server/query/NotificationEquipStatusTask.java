package com.ev49.evcs.server.query;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.domain.EquipChargeStatusInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import com.ev49.evcs.server.query.api.CDBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class NotificationEquipStatusTask extends CDBaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;

    // 存放已开始充电的订单号
    private Set<String> endTimeIsNull = new HashSet<>();

    @Scheduled(cron = "0/50 * * * * ?")
    public void equipStatus() throws IOException {
        List<ChargeOrderInfo> chargeOrderInfoList = chargeOrderInfoRepository.findByEndTimeIsNullAndStartTimeIsNotNull();

        for (ChargeOrderInfo orderInfo : chargeOrderInfoList) {
            if (orderInfo.getStartChargeSeq() != null) {
                endTimeIsNull.add(orderInfo.getStartChargeSeq());
            }
            CommonResponse startData = notificationEquipStatus(orderInfo);
        }

        for (String startChargeSeq : endTimeIsNull) {
            if (startChargeSeq != null) {
                // 根据当前充电的订单号和结束时间查询对应订单
                List<ChargeOrderInfo> endTimeIsNotNull = chargeOrderInfoRepository.findByEndTimeIsNotNullAndStartChargeSeq(startChargeSeq);
                // 如果存在已结束的充电订单就进行推送
                if (endTimeIsNotNull.size() > 0) {
                    ChargeOrderInfo chargeOrderInfo = endTimeIsNotNull.get(0);
                    CommonResponse endData = notificationEquipStatus(chargeOrderInfo);
                    // 推送成功后清空本次在内存中的订单号
                    endTimeIsNull.remove(chargeOrderInfo.getStartChargeSeq());
                }
            }
        }

    }

    private CommonResponse notificationEquipStatus(ChargeOrderInfo orderInfo) throws IOException {
        EquipChargeStatusInfo equipChargeStatusInfo = new EquipChargeStatusInfo();
        equipChargeStatusInfo.setStartChargeSeq(orderInfo.getStartChargeSeq());
        equipChargeStatusInfo.setConnectorId(orderInfo.getConnectorID());
        equipChargeStatusInfo.setSeviceMoney(orderInfo.getTotalSeviceMoney());
        equipChargeStatusInfo.setTotalPower(orderInfo.getTotalPower());
        equipChargeStatusInfo.setElecMoney(orderInfo.getTotalElecMoney());
        equipChargeStatusInfo.setTotalMoney(orderInfo.getTotalMoney());
        equipChargeStatusInfo.setStartTime(orderInfo.getStartTime());
        equipChargeStatusInfo.setChargeModel(orderInfo.getChargeModel());

        // 设置时段数
        if (orderInfo.getSumPeriod() == null) {
            equipChargeStatusInfo.setSumPeriod(0);
        } else {
            equipChargeStatusInfo.setSumPeriod(orderInfo.getSumPeriod());
        }

        // 设置充电订单状态
        if (orderInfo.getStartChargeSeqStat() == null) {
            // 设置充电订单状态为4: 未知
            equipChargeStatusInfo.setStartChargeSeqStat(4);
        } else {
            equipChargeStatusInfo.setStartChargeSeqStat(orderInfo.getStartChargeSeqStat());
        }
        // 开始充电不会有 EndTime 产生，设置当前时间为采样时间
        if (orderInfo.getEndTime() == null && orderInfo.getStartTime() != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String changeEndTime = dateFormat.format(new Date());
            equipChargeStatusInfo.setEndTime(changeEndTime);

            // 开始充电，充电桩设备状态设置为3: 占用(充电中)
            equipChargeStatusInfo.setConnectorStatus(3);
        } else {
            equipChargeStatusInfo.setEndTime(orderInfo.getEndTime());

            // 已经产生 EndTime 设置为1:空闲状态
            equipChargeStatusInfo.setConnectorStatus(1);
        }

        // ChargeDetails 明细(数组)
        ChargeDetails details = new ChargeDetails();
        details.setDetailStartTime(orderInfo.getStartTime());
        details.setDetailEndTime(equipChargeStatusInfo.getEndTime());
        details.setElecPrice(orderInfo.getTotalMoney());
        details.setDetailSeviceMoney(orderInfo.getTotalSeviceMoney());
        details.setDetailPower(orderInfo.getTotalPower());
        details.setDetailElecMoney(orderInfo.getTotalElecMoney());
        details.setSevicePrice(orderInfo.getTotalMoney());

        ChargeDetails chargeDetails[] = {details};
        equipChargeStatusInfo.setChargeDetails(chargeDetails);

        CommonRequest commonRequest = new CommonRequest();
        String data = JSONUtil.toJSONString(equipChargeStatusInfo);
        commonRequest.setOperatorID("MA61XHT69");
        commonRequest.setData(data);
        String responseBody = ok(commonRequest, "notification_equip_charge_status");
        CommonResponse notificationEquipChargeStatusData = JSONUtil.readParams(responseBody, CommonResponse.class);
        return notificationEquipChargeStatusData;
    }

}
