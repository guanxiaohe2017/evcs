package com.ev49.evcs.server.query.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.AuthSecretToken;
import com.ev49.evcs.domain.OperatorInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.QueryTokenResponse;
import com.ev49.evcs.dto.TokenRequest;
import com.ev49.evcs.repository.AuthSecretTokenRepository;
import com.ev49.evcs.repository.OperatorInfoRepository;
import com.ev49.evcs.server.exception.ServerInternalException;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController()
public class QueryTokenController extends BaseController {


    @Resource
    private AuthSecretTokenRepository authSecretTokenRepository;
    @Resource
    private OperatorInfoRepository operatorInfoRepository;

    @PostMapping("/v1/query_token")
    public QueryTokenResponse queryToken(@RequestBody CommonRequest<TokenRequest> commonRequest, @RequestHeader String outOperatorID) throws IOException {

        String data = commonRequest.getData();
        TokenRequest tokenRequest = JSONUtil.readParams(data, TokenRequest.class);
        //这里区分一电和成都市平台token
        List<OperatorInfo> operatorInfos = operatorInfoRepository.findByOperatorID(outOperatorID);
        String url = "";
        if (operatorInfos.get(0) != null && operatorInfos.get(0).getOperatorRegAddress() != null) {
            url = operatorInfos.get(0).getOperatorRegAddress();
        }

        String responseBody = ok(commonRequest, "query_token", url);

        QueryTokenResponse queryTokenResponse = DTOJsonHelper.parseResponseData(responseBody, QueryTokenResponse.class);
        AuthSecretToken authSecretToken = authSecretTokenRepository.findByOperatorIdAndSecretTokenType(outOperatorID, AuthSecretToken.SECRET_TOKEN_TYPE_OUT).orElse(new AuthSecretToken(outOperatorID, AuthSecretToken.SECRET_TOKEN_TYPE_OUT));
        if (!tokenRequest.getOperatorSecret().equals(authSecretToken.getSecret())) {
            throw new ServerInternalException("密钥错误");
        }
        DateTime dt = DateTime.now();
        String accessToken = queryTokenResponse.getAccessToken();
        if (accessToken != null) {
            authSecretToken.setToken(accessToken);
            dt = dt.plusSeconds(queryTokenResponse.getTokenAvailableTime());
            Date tokenExpiry = dt.toDate();
            authSecretToken.setTokenExpiry(tokenExpiry);
            authSecretTokenRepository.save(authSecretToken);
        }
        return queryTokenResponse;
    }

}
