package com.ev49.evcs.server.query.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.dto.ChargeInfoRequest;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.StationStatusInfo;
import com.ev49.evcs.dto.StationStatusRequest;
import com.ev49.evcs.repository.ConnectorStatusInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController()
public class ConnectorStatusInfoController extends BaseController {

    @Autowired
    ConnectorStatusInfoRepository connectorStatusInfoRepository;

    @PostMapping("/v1/query_connector_status_info")
    public ConnectorStatusInfo queryStationStatus(@RequestBody CommonRequest<ConnectorStatusInfo> commonRequest) throws IOException {

        ConnectorStatusInfo data = JSONUtil.readParams(commonRequest.getData(),ConnectorStatusInfo.class);
        String connectorID = data.getConnectorID();

        return connectorStatusInfoRepository.findById(connectorID).orElse(null);
    }

}
