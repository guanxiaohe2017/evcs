package com.ev49.evcs.server.query.api;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.server.exception.ForbiddenException;
import com.ev49.evcs.server.exception.ServerInternalException;
import com.ev49.evcs.server.query.config.EvcsInterceptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.ev49.evcs.server.query.config.EvcsInterceptor.JSON;

@Slf4j
public abstract class BaseController {

    @Resource
    private EvcsInterceptor evcsInterceptor;

    @Value("${ev49.evcs.srv.url}")
    private String evcsSrvUrl;
    @Value("${ev49.evcs.okhttp.log}")
    private boolean oklog = false;

    public String ok(Object object, String url) {
        okhttp3.RequestBody body;
        try {
            body = okhttp3.RequestBody.create(JSON, JSONUtil.toJSONString(object));
        } catch (JsonProcessingException e) {
            String msg = e.getMessage();
            log.error(msg);
            throw new ServerInternalException(msg);
        }
        Request request = new Request.Builder()
                .url(getEvcsSrvUrl() + url)
                .post(body)
                .build();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        Proxy proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved("127.0.0.1", 8888));
//        builder.proxy(proxy);
        builder.addInterceptor(evcsInterceptor);
        if (oklog) {
            builder.addInterceptor(logging);
        }
        builder.connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return fwdToInfra(request, client);
    }

    public String ok(Object object, String url,String address) {
        okhttp3.RequestBody body;
        if (!"".equals(address)) {
            setEvcsSrvUrl(address);
        }
        try {
            body = okhttp3.RequestBody.create(JSON, JSONUtil.toJSONString(object));
        } catch (JsonProcessingException e) {
            String msg = e.getMessage();
            log.error(msg);
            throw new ServerInternalException(msg);
        }
        Request request = new Request.Builder()
                .url(getEvcsSrvUrl() + url)
                .post(body)
                .build();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        Proxy proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved("127.0.0.1", 8888));
//        builder.proxy(proxy);
        builder.addInterceptor(evcsInterceptor);
        if (oklog) {
            builder.addInterceptor(logging);
        }
        builder.connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return fwdToInfra(request, client);
    }

    private String fwdToInfra(Request request, OkHttpClient client) {
        String responseBody = null;
        try {
            Response response = client.newCall(request).execute();
            if (response != null && response.isSuccessful()) {
                responseBody = response.body().string();
            } else {
                int code = response.code();
                if (code == 401 || code == 403) {
                    throw new ForbiddenException("Remote system returned " + code);
                }
            }
        } catch (IOException e) {
            String msg = e.getMessage();
            log.error(msg);
            throw new ServerInternalException(msg);
        }
        return responseBody;
    }

    public String getEvcsSrvUrl() {
        return evcsSrvUrl;
    }

    public void setEvcsSrvUrl(String evcsSrvUrl) {
        this.evcsSrvUrl = evcsSrvUrl;
    }
}
