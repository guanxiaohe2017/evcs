package com.ev49.evcs.server.query.api;

import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.EquipAuthRequest;
import com.ev49.evcs.dto.EquipChargeStatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController()
public class EquipAuthController extends BaseController {

    @PostMapping("/v1/query_equip_auth")
    public EquipChargeStatusResponse queryEquipAuth(@RequestBody CommonRequest<EquipAuthRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_equip_auth");
        EquipChargeStatusResponse chargeStatusResponse = DTOJsonHelper.parseResponseData(responseBody, EquipChargeStatusResponse.class);
        return chargeStatusResponse;
    }

}
