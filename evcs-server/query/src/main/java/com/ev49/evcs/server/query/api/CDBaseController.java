package com.ev49.evcs.server.query.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public abstract class CDBaseController extends BaseController{

    @Value("${ev49.cdevcs.srv.url}")
    public String evcsSrvUrl;

    public String getEvcsSrvUrl() {
        return evcsSrvUrl;
    }

    public void setEvcsSrvUrl(String evcsSrvUrl) {
        this.evcsSrvUrl = evcsSrvUrl;
    }
}
