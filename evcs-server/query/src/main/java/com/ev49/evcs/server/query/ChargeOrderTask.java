package com.ev49.evcs.server.query;

import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.ChargeOrderInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.QueryOrderInfoRequestData;
import com.ev49.evcs.repository.ChargeOrderInfoRepository;
import com.ev49.evcs.server.query.api.BaseController;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Component
public class ChargeOrderTask extends BaseController {

    @Autowired
    private ChargeOrderInfoRepository chargeOrderInfoRepository;
    DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");//2018-08-27 10:00:00

    @Scheduled(fixedRate = 180 * 60 * 1000)
    public void syncOrders() throws JsonProcessingException {
        syncOrders(null);
    }

    public void syncOrders(String start) throws JsonProcessingException {
        ChargeOrderInfo lastSaved = chargeOrderInfoRepository.findFirstByOrderByStartTimeDesc();
        String startTime;
        DateTime dt = new DateTime();
        String endTime = dt.toString(dtf);
        if (lastSaved != null && lastSaved.getStartTime() != null) {
            startTime = lastSaved.getStartTime();
            startTime = dtf.parseDateTime(startTime).toString(dtf);
        } else {
            dt = dt.withMillisOfDay(0).minusDays(100);
            startTime = dt.toString(dtf);
        }
        if (!StringUtils.isEmpty(start)) {
            startTime = start;
        }
        QueryOrderInfoRequestData orderInfoRequestData = new QueryOrderInfoRequestData();
        orderInfoRequestData.setStartTime(startTime);
        orderInfoRequestData.setEndTime(endTime);
        CommonRequest<QueryOrderInfoRequestData> request = new CommonRequest<>();
        request.setOperatorID("MA61XHT69");
        String requestData = JSONUtil.toJSONString(orderInfoRequestData);
        request.setData(requestData);
        List<ChargeOrderInfo> chargeOrderInfos = queryOrderInfo(request);
        if (chargeOrderInfos != null) {
            chargeOrderInfoRepository.saveAll(chargeOrderInfos);
        }
    }

    public List<ChargeOrderInfo> queryOrderInfo(CommonRequest<QueryOrderInfoRequestData> commonRequest) {
        String responseBody = ok(commonRequest, "query_order_info");
        List<ChargeOrderInfo> chargeOrderInfos = DTOJsonHelper.parseResponseDataAsList(responseBody, ChargeOrderInfo.class);
        return chargeOrderInfos;
    }
}
