package com.ev49.evcs.server.query.api;

import com.ev49.evcs.domain.ConnectorStatusInfo;
import com.ev49.evcs.dto.*;
import com.ev49.evcs.server.query.NotificationConnectorStatusTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.ev49.evcs.domain.EquipChargeStatusInfo;
import com.ev49.evcs.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController()
public class EquipChargeStatusController extends BaseController {

    @Autowired
    NotificationConnectorStatusTask statusTask;

    @PostMapping("/v1/query_equip_charge_status")
    public EquipChargeStatusResponse queryChargeStatus(@RequestBody CommonRequest<ChargeStatusRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_equip_charge_status");
        EquipChargeStatusResponse chargeStatusResponse = DTOJsonHelper.parseResponseData(responseBody, EquipChargeStatusResponse.class);
        return chargeStatusResponse;
    }
    /**
     * 调用市级平台的notification_stationStatus接口 推送设备状态变化，返回接收方接收状态
     * @param connectorStatusInfoRequest
     * @return
     */
    @PostMapping("/v1/notification_stationStatus_2_city")
    public Status notificationStationStatus(@RequestBody ConnectorStatusInfo connectorStatusInfoRequest) throws JsonProcessingException {

        return statusTask.notificationStationStatus(connectorStatusInfoRequest);
    }

}
