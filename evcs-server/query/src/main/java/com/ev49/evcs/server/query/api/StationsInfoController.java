package com.ev49.evcs.server.query.api;

import com.ev49.evcs.domain.StationInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.DTOJsonHelper;
import com.ev49.evcs.dto.PageRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController()
public class StationsInfoController extends BaseController {

    @PostMapping("/v1/query_stations_info")
    public List<StationInfo> queryStationsInfo(@RequestBody CommonRequest<PageRequest> commonRequest) {
        String responseBody = ok(commonRequest, "query_stations_info");
        List<StationInfo> stationList = DTOJsonHelper.parseResponseDataAsList(responseBody, StationInfo.class, "StationInfos");
        return stationList;
    }

}
