package com.ev49.evcs.server.query.config;

import com.ev49.common.utils.encryption.Aes128Cbc;
import com.ev49.common.utils.encryption.HMAC;
import com.ev49.common.utils.encryption.Seq;
import com.ev49.common.utils.json.JSONUtil;
import com.ev49.evcs.domain.AuthSecretToken;
import com.ev49.evcs.domain.OperatorInfo;
import com.ev49.evcs.dto.CommonRequest;
import com.ev49.evcs.dto.QueryTokenResponse;
import com.ev49.evcs.repository.AuthSecretTokenRepository;
import com.ev49.evcs.repository.OperatorInfoRepository;
import com.ev49.evcs.server.exception.ServerInternalException;
import com.ev49.evcs.server.query.api.QueryTokenController;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.EntityNotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class EvcsInterceptor implements Interceptor {

    public static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");

    @Resource
    OperatorInfoRepository operatorInfoRepository;
    @Resource
    AuthSecretTokenRepository authSecretTokenRepository;
    @Resource
    private QueryTokenController queryTokenController;

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        String secret = "6B1E87D6310D835A";
        Response originResponse;
        Request originalRequest = chain.request();
        RequestBody requestBody = originalRequest.body();
        if (requestBody == null) {
            originResponse = chain.proceed(originalRequest);
        } else {
            try {
                String outHost = originalRequest.url().host();
                List<OperatorInfo> outOperators = operatorInfoRepository.findByOperatorRegAddressLike("%"+outHost+"%");
                if (outOperators.size()==0) {
                    throw new ServerInternalException("未查询到运营商信息");
                }
                String outOperatorID = outOperators.get(0).getOperatorID();
                Request encryptedRequest = encrypt(originalRequest, secret, outOperatorID);
                originResponse = chain.proceed(encryptedRequest);
            } catch (Exception e) {
                String msg = e.getMessage();
                log.error(msg);
                throw new IOException(msg);
            }
        }
        ResponseBody responseBody = originResponse.body();
        if (originResponse.isSuccessful() && responseBody != null) {
            Response.Builder builder = originResponse.newBuilder();
            try {
                ResponseBody decryptedBody = decrypt(originResponse, secret);
                builder.body(decryptedBody);
                return builder.build();
            } catch (Exception e) {
                String msg = e.getMessage() + "|Body decryption fail.";
                log.error(msg);
                builder.code(500);
                return builder.build();
//                throw new IOException(msg); todo delete or use?
            }
        } else {
            return originResponse;
        }

    }

    private ResponseBody decrypt(Response response, String secret) throws IOException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {
        ObjectMapper objectMapper = new ObjectMapper();
        byte[] buf = response.body().bytes();
        JsonNode rootNode = objectMapper.readTree(buf);
        JsonNode dataNode = rootNode.path("Data");
        if (!dataNode.isNull()) {
            String rawData = dataNode.asText();
            String decryptedData = Aes128Cbc.decryptString(rawData, secret);
            ((ObjectNode) rootNode).put("Data", decryptedData);
            String decryptedBody = rootNode.toString();
            MediaType contentType = response.body().contentType();
            return ResponseBody.create(contentType, decryptedBody);
        }
        return response.body();
    }

    private Request encrypt(Request request, String secret, String outOperatorID) throws IOException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {
        Request.Builder builder;
        RequestBody newRequestBody;
        if (request.body() != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Sink sink = Okio.sink(baos);
            BufferedSink bufferedSink = Okio.buffer(sink);
//            request.body().writeTo(bufferedSink); //old body
            String rawbody = bodyToString(request);
            CommonRequest originalRequest = readCommonRequest(rawbody);
            String encryptData = Aes128Cbc.encrypt(originalRequest.getData(), secret);
            CommonRequest commonRequest = new CommonRequest();
            commonRequest.setData(encryptData);
            String operatorID = originalRequest.getOperatorID();
            commonRequest.setOperatorID(operatorID);
            commonRequest.setTimeStamp(ts());
            commonRequest.setSeq(Seq.getNextNIncrease());
            commonRequest.setSig(HMAC.hmacDigest(operatorID, commonRequest.getData(), commonRequest.getTimeStamp(), commonRequest.getSeq(), secret));
            String wrapbody = JSONUtil.toJSONString(commonRequest);
            bufferedSink.writeString(wrapbody, Charset.defaultCharset());
            newRequestBody = RequestBody.create(request.body().contentType(), bufferedSink.buffer().readUtf8());
            builder = request.newBuilder();
            if (!request.url().encodedPath().endsWith("/query_token")) {
                builder.header("Authorization", "Bearer " + getToken(outOperatorID, secret));
            }
            builder.post(newRequestBody);
            return builder.build();
        } else {
            return request;
        }
    }

    private String getToken(String outOperatorID, String secret) throws IOException {

        operatorInfoRepository.findById(outOperatorID).orElseThrow(() -> new EntityNotFoundException("OperatorInfo::OperatorID[" + outOperatorID + "] not found."));
        AuthSecretToken authSecretToken = authSecretTokenRepository.findByOperatorIdAndSecretTokenType(outOperatorID, AuthSecretToken.SECRET_TOKEN_TYPE_OUT).orElse(new AuthSecretToken());
        Date tokenExpiry = authSecretToken.getTokenExpiry();
        DateTime dt = DateTime.now();
        String token;
        if (tokenExpiry == null || tokenExpiry.before(dt.toDate())) {
            CommonRequest req = new CommonRequest();
            req.setOperatorID("MA61XHT69");
            req.setData("{\"OperatorID\":\"MA61XHT69\",\"OperatorSecret\":\"" + secret + "\"}");
            QueryTokenResponse queryTokenResponse = queryTokenController.queryToken(req, outOperatorID);
            token = queryTokenResponse.getAccessToken();
        } else {
            token = authSecretToken.getToken();
        }
        return token;
    }

    private CommonRequest readCommonRequest(String rawbody) {
        try {
            return JSONUtil.readParams(rawbody, CommonRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new ServerInternalException("Illegal param: " + rawbody);
    }

    private static String fmt = "yyyyMMddHHmmss";

    private String ts() {
        Calendar calendar = Calendar.getInstance();
        DateTime dateTime = new DateTime(calendar);
        String ts = dateTime.toString(fmt);
        return ts;
    }

    private static String bodyToString(final Request request) throws IOException {
        final Request copy = request.newBuilder().build();
        final Buffer buffer = new Buffer();
        copy.body().writeTo(buffer);
        return buffer.readUtf8();
    }
}

